# README #

### What is this repository for? ###

SpaceGun. Android source for sampler. Java, C++. Requires MayaswellCore and SpaceGunLib

### How do I get set up? ###

Download, open in Android Studio, build with Gradle.

### Who do I talk to? ###

David Karla, dak@mayaswell.com