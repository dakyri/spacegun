package com.mayaswell.spacegun;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.RunnableFuture;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Bufferator.BufferatorException;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Controllable.ControllableManager;
import com.mayaswell.audio.Controllable.SensorControlManager;
import com.mayaswell.audio.Controllable.ControllableInfo;
import com.mayaswell.audio.Controllable.ControllerInfo;
import com.mayaswell.audio.Controllable.SXControlAssign;
import com.mayaswell.audio.Controllable.SensorInfo;
import com.mayaswell.audio.Controllable.XYControlAssign;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.FX.Host;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.audio.PadMode;
import com.mayaswell.audio.file.AudioFile;
import com.mayaswell.util.ErrorLevel;
import com.mayaswell.util.MWActivity;
import com.mayaswell.util.MWBPActivity;
import com.mayaswell.util.MWUtils;
import com.mayaswell.widget.FileChooserView;
import com.mayaswell.widget.MenuData;
import com.mayaswell.widget.MultiModeButton;
import com.mayaswell.widget.PrimeableButton;
import com.mayaswell.widget.TabWedgeIt;
import com.mayaswell.widget.TeaPot;
import com.mayaswell.widget.TabWedgeIt.TabWedgeItListener;
import com.mayaswell.widget.TeaPot.TeaPotListener;
import com.mayaswell.fragment.MWFragment;
import com.mayaswell.fragment.ModControlsFragment;
import com.mayaswell.util.AbstractPatch.Action;
import com.mayaswell.util.BankSanitizer;
import com.mayaswell.util.EventID;
import com.mayaswell.util.SimpleFileChooser;
import com.mayaswell.spacegun.fragment.BusControlsFragment;
import com.mayaswell.spacegun.fragment.FragmentHolder;
import com.mayaswell.spacegun.fragment.MainControlsFragment;
import com.mayaswell.spacegun.fragment.SampleEditorFragment;
import com.mayaswell.spacegun.fragment.SensorAssignFragment;
import com.mayaswell.spacegun.fragment.TouchBayFragment;
import com.mayaswell.spacegun.widget.PadButton;
import com.mayaswell.spacegun.widget.RibbonController;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import rx.Observer;
import rx.functions.Action1;

public class SpaceGun extends MWBPPadActivity<Bank, Patch, CControl> implements SensorEventListener, ControllableManager, SensorControlManager
{
	/**
	 * constants that we want to export from here into C++
	 *
	 */
	public class Global {
		public static final int LFO_PER_PAD = 4;
		public static final int MAX_LFO_TGT = 4;
		public static final int ENV_PER_PAD = 2;
		public static final int MAX_ENV_TGT = 4;
		public static final int STPR_PER_PAD = 2;
		public static final int MAX_STPR_TGT = 4;
		public static final int MAX_BUS = 4;
		public static final int MAX_BUS_FX = 4;
		public static final int MAX_FX_PARAM = 6;
	}
	
	protected ArrayList<PadButton> padButton = null;
	protected ArrayList<PadSample> padSample = null;
	protected View buttonSelectHilight = null;
	 
	public ArrayAdapter<MenuData<PadMode>> padModeAdapter = null;
	
	public RibbonController ribbonController = null;
	public PrimeableButton saveSelectButton = null; // public ... the touchpad needs to check this too
	protected PrimeableButton recButton = null; // public ... the touchpad needs to check this too
	protected TeaPot patchTempoControl = null;
	
	protected  EditText saveFileEditText = null;

	protected FragmentHolder fragmentHolder = null;
	
//	protected RelativeLayout controllerEditor = null;

	private SampleEditorFragment sampleEditorFragment = null;
	private MainControlsFragment mainControlsFragment = null;
	private ModControlsFragment<SpaceGun> modControlsFragment = null;
	private BusControlsFragment busControlsFragment = null;
	private SensorAssignFragment sensorAssignFragment = null;
	private TouchBayFragment touchBayFragment = null;
	private TabWedgeIt fragmentSelector = null;
//	private View padFilterEditorBody = null;
	
	//	private ArrayList<ModulatorControl> modControl = null;
	
//	protected View mainControls = null;
	
	protected PadButton recPadButton=null;
	protected PadSample recSample=null;
	protected boolean recSampleWasPlaying = false;
	protected File recFile=null;
	private int lastRecFileNumber=0;
	
	protected PadButton selectedPadButton=null;
	public PadSample selectedPadSample=null;

	private SensorManager sensorManager = null;
	private Sensor accelerometer = null;
	private Sensor magnetometer = null;
	private Sensor gyro = null;
	private Sensor luxometer = null;
	private Sensor proxometer = null;
	private Sensor rotometer = null;
	
	private SharedPreferences sgPreferences = null;
	private boolean keepScreenAlive = true;
	
	private static ArrayAdapter<ControllableInfo> controllableAdapter = null;
	
	public static SpaceGun sg = null;
	public static ArrayAdapter<SensorInfo> sensorAdapter = null;
	public static ArrayAdapter<ControllerInfo> ribbonAdapter = null;
	public static ArrayAdapter<FX> fxAdapter = null;
	
	public ArrayList<FX> fxList = null;
	
	protected boolean shouldPrimeEditButton = false;
	
	public boolean showSampleTimeBeats = false;
	private boolean automaticBankSanitize = false;
	
	protected SGAudioMixer player = null;
	public BusMixer busMixer = null;
	
	public SGOSCModule oscModule = null;
	protected BankSanitizer bankSanitizer = null;

	protected Bufferator bufferator = null;
	
	public SpaceGun() {
		super(".sgx");
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		CControl.init(
				Global.LFO_PER_PAD, Global.MAX_LFO_TGT, Global.ENV_PER_PAD, Global.MAX_ENV_TGT,
				Global.MAX_BUS, Global.MAX_BUS_FX, Global.MAX_FX_PARAM);
		SpaceGun.sg = this;

		String packageName = getPackageName();
		Resources r = getResources();
		controllableAdapter = new ArrayAdapter<ControllableInfo>(this, R.layout.mw_spinner_item);
		controllableAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		sensorAdapter = new ArrayAdapter<SensorInfo>(this, R.layout.mw_spinner_item);
		sensorAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		ribbonAdapter = new ArrayAdapter<ControllerInfo>(this, R.layout.mw_spinner_item);;
		ribbonAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);

		setContentView(R.layout.main);
		
		doPreferences();
		setupSensors();

		PadSample.globalCPadInit();
		BusMixer.globalBmxInit();
		if (bufferator == null) {
			bufferator = Bufferator.init(this);
			final MWBPActivity<?,?,?> activity = this;
			bufferator.monitorState(new Action1<BufferatorException>() {
				@Override
				public void call(BufferatorException e) {
					if (e.severity == ErrorLevel.FNF_ERROR_EVENT) {
						String si = e.getPath();
						bankSanitizer = BankSanitizer.getInstance();
						if (!BankSanitizer.isChecking()) {
							BankSanitizer.check(currentBank, activity, currentPatch, si);
						}
					} else {
						if (errorIsShowing) {
							Log.d("error", "skip formal display of error " + e.getMessage());
						} else {
							handleErrorMessage(e.getMessage(), e.severity);
						}
					}
				}
			});
			bufferator.monitorGfx(new Action1<SampleInfo>() {
				@Override
				public void call(SampleInfo sampleInfo) {
					if (selectedPadSample != null && selectedPadSample.hasSample(sampleInfo)) {
						if (sampleEditorFragment != null) {
							sampleEditorFragment.setSampleGfx4Pad(selectedPadSample);
						}
					}
				}
			});
		}
/*
 *  find our main pads and make basis synthesis components
 */
		padButton = new ArrayList<PadButton>();
		padSample = new ArrayList<PadSample>();
		int resId = -1;
		String name = null;
		int i = 1;
		name = "padButton"+Integer.toString(i);
		
		buttonSelectHilight = findViewById(R.id.buttonSelectHilight);
		while ((resId=r.getIdentifier(name, "id", packageName)) > 0) {
			PadButton pb = (PadButton) findViewById(resId);
			PadSample pbs = new PadSample(this, "Pad "+Integer.toString(i), i);
			pb.setListener(new PadButton.Listener() {
				@Override
				public void onStateChanged(MultiModeButton b, int mode) {
					PadButton pb = null;
					PadSample pbs = null;
					try {
						pb = (PadButton) b;
						pbs = pb.getSample();
					} catch (ClassCastException e1) {
						return;
					}
					switch (mode) {
					case MultiModeButton.ACTIVATED: {
						if (recButton.isPressed() || recButton.getPrimedState()) {
							startRecording(pb);
							recButton.setPrimedState(false);
						} else if (saveSelectButton.isPressed() || saveSelectButton.getPrimedState()) {
							selectPad(pb);
						} else if (pbs.isPaused()) {
							stopPad(pb);
						} else {
							switch (pbs.state.padMode) {
							case NORMAL: {
								startPad(pb);
								break;
							}
							case TOGGLE: {
	//							Log.d("play", Boolean.toString(pbs.isPlayable()));
								if (!player.isPlaying(pbs)) startPad(pb);
								else stopPad(pb);
								break;
							}
							case MANUAL: {
								startPad(pb);
								break;
							}
							}
						}
						break;
					}
					case MultiModeButton.DEACTIVATED: {
						switch (pbs.state.padMode) {
						case NORMAL: {
							break;
						}
						case TOGGLE: {
	//						if (pbs.isPlaying()) stopPad(pb);
							break;
						}
						case MANUAL: {
							stopPad(pb);
							break;
						}
						}
						break;
					}
					}
				}
			});
			pbs.setListener(new PadSample.Listener() {
				@Override
				public void playStarted(final PadSample p) {// we've started it elsewhere, just want to update gfx
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							((PadButton)p.padButton).setPlayState(true);
						}
					});
				}

					/*
					case PadSample.PLAY_START_SYNCED: {
// we want to start it elsewhere and we don't want a synchronization clusterfuck there is a messaging lag here though
						player.startPad(pbs);
						pb.setPlayState(pbs.isPlaying());
						return true;
					}*/

				@Override
				public void playComplete(PadSample padSample) {
					stopPad((PadButton) padSample.padButton);
				}

				@Override
				public void setSampleData(PadSample padSample) {
					if (sampleEditorFragment != null) {
						sampleEditorFragment.setToPad(selectedPadSample);
					}
				}
			});
			pb.setSample(pbs);
			pbs.setPadButton(pb);
			padButton.add(pb);
			padSample.add(pbs);
			
			name = "padButton"+Integer.toString(++i);
		}
		for (PadSample p : padSample) {
			p.setupSyncAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);
			p.setupControlsAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);
			controllableAdapter.add(new ControllableInfo(p, p.name));
		}
		PadSample.setupFiltersAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);
		
		bufferator.setSampleCacheSize(padSample.size());

		sensorAdapter.add(new SensorInfo(SensorInfo.ROT_X, "Rot X", "rotX"));
		sensorAdapter.add(new SensorInfo(SensorInfo.ROT_Y, "Rot Y", "rotY"));
		sensorAdapter.add(new SensorInfo(SensorInfo.ROT_Z, "Rot Z", "rotZ"));
		
		ribbonAdapter.add(new ControllerInfo(0, "Ribbon 1", "ribbon1"));
		
/*
 *  create player and banks
 */
		player = new SGAudioMixer(padSample.size());

		defaultBankFileName = r.getString(R.string.default_bank);	
		currentBank = loadBank(defaultBankFileName, true, false);
		if (currentBank == null) {
			currentBank = new Bank(padSample.size());
			saveBank(currentBank, defaultBankFileName, true);
		} else {
		}
		if (currentBank.patch.size() == 0) {
			currentBank.add(currentBank.newPatch());
		}
		
		for (PadSample p: padSample) {
			p.setBufsize(player.getOutBufsize());
		}
	
/*
 *  find all out main interface bits
 */
		ribbonController = (RibbonController) findViewById(R.id.ribbonController);
		patchSelector = (Spinner) findViewById(R.id.patchSelector);
		patchNameEditView = (EditText) findViewById(R.id.patchNameEditView);
		saveSelectButton = (PrimeableButton) findViewById(R.id.selectButton);
		recButton = (PrimeableButton) findViewById(R.id.recButton);
		patchTempoControl = (TeaPot) findViewById(R.id.patchTempoControl);
		
		fragmentHolder = (FragmentHolder) findViewById(R.id.operationalViews);
		fragmentSelector = (TabWedgeIt) findViewById(R.id.fragmentSelector);
		/*
		mainControlsFragment = (MainControlsFragment) getFragmentManager().findFragmentById(R.id.mainControlsFragment);
		sampleEditorFragment = (SampleEditorFragment) getFragmentManager().findFragmentById(R.id.sampleEditorFragment);
		modControlsFragment = (ModControlsFragment) getFragmentManager().findFragmentById(R.id.modControlsFragment);
		busControlsFragment = (BusControlsFragment) getFragmentManager().findFragmentById(R.id.busControlsFragment);
		sensorAssignFragment = (SensorAssignFragment) getFragmentManager().findFragmentById(R.id.sensorAssignFragment);
		touchBayFragment = (TouchBayFragment) getFragmentManager().findFragmentById(R.id.touchBayFragment);
		*/
		
		mainControlsFragment = new MainControlsFragment();
		sampleEditorFragment = new SampleEditorFragment();
		modControlsFragment = new ModControlsFragment<SpaceGun>();
		busControlsFragment = new BusControlsFragment();
		sensorAssignFragment = new SensorAssignFragment();
		touchBayFragment = new TouchBayFragment();

		fragmentSelector.add("main", 0, null, mainControlsFragment);
		fragmentSelector.add("smp", 1, null, sampleEditorFragment);
		fragmentSelector.add("mod", 2, null, modControlsFragment);
		fragmentSelector.add("bus", 3, null, busControlsFragment);
		fragmentSelector.add("touch", 4, null, touchBayFragment);
		fragmentSelector.add("ctlrs", 5, null, sensorAssignFragment);
		fragmentSelector.requestLayout();

		Log.d("fragsel", fragmentSelector.getOrientVertical()+" orient vertical");
		
		fragmentSelector.setListener(new TabWedgeItListener() {
			@Override
			public void onTabReselected(int ind, Object d) {
			}

			@Override
			public void onTabSelected(int ind, Object d) {
				try {
					fragmentHolder.showFragment((MWFragment) d);
				} catch (ClassCastException e) {
					
				}
			}

			@Override
			public void onTabUnselected(int ind, Object d) {
				try {
					fragmentHolder.hideFragment((Fragment) d);
				} catch (ClassCastException e) {
					
				}
			}
			
		});

//		mainControls = findViewById(R.id.mainControls);

		padModeAdapter = new ArrayAdapter<MenuData<PadMode>>(this, R.layout.mw_spinner_item);
		padModeAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		padModeAdapter.add(new MenuData<PadMode>(PadMode.NORMAL, "Normal"));
		padModeAdapter.add(new MenuData<PadMode>(PadMode.TOGGLE, "Toggle"));
		padModeAdapter.add(new MenuData<PadMode>(PadMode.MANUAL, "Manual"));
		
		fxList = MWUtils.defaultFX();
		if (fxAdapter == null) {
			fxAdapter = new ArrayAdapter<FX>(this, R.layout.mw_spinner_item);
			fxAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
			for (FX f: fxList) {
				fxAdapter.add(f);
			}
		}
/** defunct code, but an interesting solution that may be handy later
		saveEditButton.setOnTouchListener(new OnTouchListener() {
			private PadButton buttonEdittedWhenPressed = null;

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				int action = e.getActionMasked();
				if (action == MotionEvent.ACTION_DOWN) {
					buttonEdittedWhenPressed = selectedPadButton;
				} else if (action == MotionEvent.ACTION_UP) {
					Rect vr = new Rect();
					v.getDrawingRect(vr);
					vr.offsetTo(0, 0);
					if (vr.contains((int)e.getX(), (int)e.getY())) {
						if (buttonEdittedWhenPressed == selectedPadButton) {
							saveEdit();
						}
					}
					buttonEdittedWhenPressed = null;
				}
				return false;
			}
		});
*/
						
/* main page controls */
		setupPatchControls();

		saveSelectButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				int action = e.getActionMasked();
				if (action == MotionEvent.ACTION_DOWN) {
					shouldPrimeEditButton = true;
				} else if (action == MotionEvent.ACTION_UP) {
					if (saveSelectButton.getPrimedState()) {
						saveSelectButton.setPrimedState(false);
					} else {
						if (shouldPrimeEditButton) {
							saveSelectButton.setPrimedState(true);
						}
					}
				}
				return false;
			}
		});
		saveSelectButton.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				savePatch(currentPatchInd, currentPatch);
				return false;
			}
			
		});
		
		patchTempoControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				Patch p;
				setTempo(v);
				Log.d("tempo set", String.format("%g tempo", v));
				if ((p=getCurrentPatch()) != null) {
					p.setTempo(v);
				}
			}
		});
		
		recButton.setOnTouchListener(new OnTouchListener() {
			private boolean recPressedWhileRecord = false;

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				int action = e.getActionMasked();
				if (action == MotionEvent.ACTION_DOWN) {
					if (player.isRecording()) {
						recPressedWhileRecord  = true;
					} else {
						recPressedWhileRecord = false;
					}
				} else if (action == MotionEvent.ACTION_UP) {
					Rect vr = new Rect();
					v.getDrawingRect(vr);
					vr.offsetTo(0, 0);
					if (vr.contains((int)e.getX(), (int)e.getY())) {
						if (recPressedWhileRecord) {
							if (player.isRecording()) {
								stopRecording();
							}
						} else {
							if (!player.isRecording()) {
								recButton.setPrimedState(!recButton.getPrimedState());
							}
						}
					}
					recPressedWhileRecord = false;
				}
				return false;
			}
		});
		
		oscModule = new SGOSCModule(this);

		saveFileEditText = new EditText(this);
		
		Log.d("creation", "done osc setup");
		busMixer = BusMixer.build(Global.MAX_BUS, Global.MAX_BUS_FX, Global.MAX_FX_PARAM, Global.LFO_PER_PAD, Global.MAX_LFO_TGT, player.getOutBufsize());
		
		Log.d("creation", "built bus global");
		for (Bus b: busMixer.bus) {
			b.setupControlsAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);
			controllableAdapter.add(new ControllableInfo(b, b.toString()));
		}
		Log.d("creation", "done bus setup");
		busControlsFragment.setBusList(busMixer.getBus());
		
		Log.d("creation", "done bus list setup");
		setCurrentPatchInd(0);
		
		Log.d("creation", "settting fragment selector");
		fragmentSelector.setMultiSelect(getResources().getInteger(R.integer.max_fragment_selectable));

		Log.d("creation", "done settting fragment selector");
		selectPad(padButton.get(0));
		Log.d("creation", "complete");
	}

	/**
	 * The activity is about to become visible.
	 */
	@Override
	protected void onStart()
	{
		Log.d("space gun", "onStart");
		
		if (fragmentSelector.countSelected() == 0) fragmentSelector.selectTab(0);
		player.play();
		bufferator.run();
		super.onStart();
		startPeriodicUiUpdate();
	}
	
	/** 
	 * The activity has become visible (it is now "resumed").
	 */
   @Override
   protected void onResume()
   {
		Log.d("space gun", "onResume");
		super.onResume();
		resumeSensors();
		startPeriodicUiUpdate();
   }
	
   /**
	*  Another activity is taking focus (this activity is about to be "paused").
	*/
	@Override
	protected void onPause()
	{
		Log.d("space gun", "onPause");
		super.onPause();
		pauseSensors();
		stopPeriodicUiUpdate();
		Log.d("space gun", "onPause done");
	}
	
	/**
	 * The activity is no longer visible (it is now "stopped")
	 */
	@Override
	protected void onStop()
	{
		Log.d("space gun", "onStop");
		super.onStop();
		pauseSensors();
		stopPeriodicUiUpdate();
		Log.d("space gun", "onStop done");
	}
	
	/**
	 * The activity is about to be destroyed.
	 */
	@Override
	protected void onDestroy()
	{
		Log.d("OnDestroy", "entering destruction");
		player.stop();// which will close it and force all the cleanups
		if (player.isRecording()) {
			player.stopRecording();
		}
		sensorManager.unregisterListener(this);
		PadSample.globalCPadCleanup();
		BusMixer.globalBmxCleanup();
		bufferator.stop();
		bufferator.cleanup();
		Looper l = getMainLooper();
		Log.d("OnDestroy", "looper thread "+l.getThread().getId());
		super.onDestroy();
		Log.d("OnDestroy", "is done ok");
	}

	/**
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed()
	{
		bailAlog("Save current bank before exit?");
		if (bankSanitizer != null) {
			bankSanitizer.finish();
		}
//		super.onBackPressed();
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		// TODO maybe we should work out our screen width and force action menu items to the bar ... 
		// com.android.internal.view.ActionBarPolicy
		//public int  [More ...] getEmbeddedMenuWidthLimit() {        return mContext.getResources().getDisplayMetrics().widthPixels / 2;
		// mActionItemWidthLimit = width in com.android.internal.view.menu.ActionMenuPresenter init for menu
		// used in flagOptionItems()
		// ie it's a system limit to stick with half the screen width
		// without title text, I can fit for on the SGII
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onPrepareOptionsMenu (Menu menu)
	{
		MenuItem miLoad = menu.findItem(R.id.sg_menu_load_bank);
		MenuItem miNew = menu.findItem(R.id.sg_menu_new_bank);
		MenuItem miSave = menu.findItem(R.id.sg_menu_save_bank);
		MenuItem miSaveState = menu.findItem(R.id.sg_menu_save_bank_and_state);
		MenuItem miSaveAs = menu.findItem(R.id.sg_menu_save_bank_as);
		MenuItem miSaveDflt = menu.findItem(R.id.sg_menu_save_bank_2dflt);
		if (miLoad != null) miLoad.setVisible(true);
		if (miNew != null) miNew.setVisible(true);
		if (miSave != null) miSave.setVisible(true);
		if (miSaveState != null) miSave.setVisible(true);
		if (miSaveAs != null) miSaveAs.setVisible(true);
		if (miSaveDflt != null) miSaveDflt.setVisible(true);

		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		switch (item.getItemId()) {
		case R.id.sg_menu_new_patch: {
			newPatch();
			if (busControlsFragment != null) {
				Log.d("new patch", "get select "+busControlsFragment.getSelectedId());
			}
			return true;
		}
		case R.id.sg_menu_save_patch: {
			setCurrentPatchState(currentPatch);
			savePatch(currentPatchInd, currentPatch);
			return true;
		}
		case R.id.sg_menu_clone_patch: {
			branchPatch();
			return true;
		}
		case R.id.sg_menu_reload_patch: {
			reloadCurrentPatch();
			return true;
		}

		case R.id.sg_menu_del_patch: {
			delPatch();
			return true;
		}
		case  R.id.sg_menu_ren_patch: {
			renPatch();
			return true;
		}

		case R.id.sg_menu_load_bank: {
			String [] fileFilter = {".*\\.sgx"};
			String startDir = getCurrentBankBaseDir();
			Intent request =new Intent(SpaceGun.this, SimpleFileChooser.class);

			request.putExtra("startDir", startDir);
			request.putExtra("fileFilter", fileFilter);
			request.putExtra("showHidden", false);
			startActivityForResult(request,  R.id.sg_menu_load_bank);
			return true;
		}
		case R.id.sg_menu_new_bank: {
			currentBank = new Bank(padSample.size());
			if (currentBank.patch.size() == 0) {
				currentBank.add(currentBank.newPatch());
			}
			currentBankFile = null;
			setCurrentPatchInd(0);
			loadPatchDisplayUpdate();
			return true;
		}
		case R.id.sg_menu_save_bank_and_state: {
			if (currentBankFile == null) {
				saveFialog("bank.sgx", null, false, true);
			} else {
				saveCurrentBank(false, true);
			}
			return true;
		}
		case R.id.sg_menu_save_bank: {
			if (currentBankFile == null) {
				saveFialog("bank.sgx", null, false, false);
			} else {
				saveCurrentBank(false, false);
			}
			return true;
		}
		case R.id.sg_menu_save_bank_as: {
			String dnm = null;
			if (currentBankFile != null) {
				dnm = currentBankFile.getName();
			} else {
				dnm = "bank.sgx";
			}
			saveFialog(dnm, null, false, false);
			return true;
		}
		case R.id.sg_menu_save_bank_2dflt: {
			if (currentBank != null) {
				setCurrentPatchState(getCurrentPatch());
				if (currentPatchInd >= 0 && currentPatchInd < currentBank.patch.size()) {
					currentBank.patch.set(currentPatchInd, getCurrentPatch());
				}
				saveBank(currentBank, defaultBankFileName, true);
			}
			return true;
		}
		case R.id.sg_menu_sanitize_bank: {
			bankSanitizer = BankSanitizer.getInstance();
			BankSanitizer.check(currentBank, this, null, null);
			return true;
		}
		case R.id.sg_menu_help: {
			showHelp();
			return true;
		}
		case R.id.sg_menu_about: {
			showAbout();
			return true;
		}
		case R.id.sg_menu_privacy: {
			showTextDialog(getResources().getString(R.string.privacy_policy_title), R.string.privacy_policy_text);
			return true;
		}
		case R.id.sg_menu_preferences: {
			Intent i = new Intent(this, SGPreferenceActivity.class);
			startActivityForResult(i, R.id.sg_menu_preferences);
			return true;
		}
		case R.id.sg_menu_exit: {
			bailAlog("Save current bank before exit?");
			return true;
		}
		default: {
//			Log.d("option", "Unhandled menu option"+Integer.toString(item.getItemId()));
		}
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onLowMemory ()
	{
		super.onLowMemory();
		Log.d("SpaceGun", String.format("on low memory"));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onTrimMemory(int level)
	{
		super.onTrimMemory(level);
		Log.d("SpaceGun", String.format("on trim memory %d", level));
	}
	
	/***************************************
	 * SENSOR RELATED METHODS
	 ***************************************/
	private void setupSensors()
	{
		sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
	}

	private void resumeSensors()
	{
		if (rotometer != null) sensorManager.registerListener(this, rotometer, SensorManager.SENSOR_DELAY_NORMAL);
		if (accelerometer != null) sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
		if (gyro != null) sensorManager.registerListener(this, gyro, SensorManager.SENSOR_DELAY_NORMAL);
		if (proxometer != null) sensorManager.registerListener(this, proxometer, SensorManager.SENSOR_DELAY_NORMAL);
		if (magnetometer != null) sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
		if (luxometer != null) sensorManager.registerListener(this, luxometer, SensorManager.SENSOR_DELAY_NORMAL);
	}

	private void pauseSensors()
	{
		if (rotometer != null) sensorManager.unregisterListener(this, rotometer);
		if (accelerometer != null) sensorManager.unregisterListener(this, accelerometer);
		if (gyro != null) sensorManager.unregisterListener(this, gyro);
		if (proxometer != null) sensorManager.unregisterListener(this, proxometer);
		if (magnetometer != null) sensorManager.unregisterListener(this, magnetometer);
		if (luxometer != null) sensorManager.unregisterListener(this, luxometer);
	}
	
	public boolean checkStartStopSensors()
	{
		Patch p = getCurrentPatch();
		if (p == null) {
			return false;
		}
		boolean hasRoto=false;
		boolean hasProx=false;
		boolean hasLux=false;
		boolean hasMag=false;
		boolean hasGyro=false;
		boolean hasAccel=false;
		for (SXControlAssign sxp: p.sensorAssign) {
			if (sxp != null) {
				switch(sxp.getSensorId()) {
				case SensorInfo.NONE: {
					break;
				}
				case SensorInfo.ROT_X: 
				case SensorInfo.ROT_Y: 
				case SensorInfo.ROT_Z: {
					if (sxp.isValid()) {
						hasRoto = true;
					}
					break;
				}
				
				
				}
			}
		}
		
		if (hasRoto) {
			if (rotometer == null) {
				rotometer = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
			}
			if (rotometer != null) {
				sensorManager.registerListener(this, rotometer, SensorManager.SENSOR_DELAY_NORMAL);
			}
		} else {
			if (rotometer != null) {
				if (rotometer != null) sensorManager.unregisterListener(this, rotometer);
				rotometer = null;
			}
		}
		if (hasProx) {
			if (proxometer == null) {
				proxometer = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
			}
			if (proxometer != null) {
				sensorManager.registerListener(this, proxometer, SensorManager.SENSOR_DELAY_NORMAL);
			}
		} else {
			if (proxometer != null) {
				if (proxometer != null) sensorManager.unregisterListener(this, proxometer);
				proxometer = null;
			}
		}
		if (hasLux) {
			if (luxometer == null) {
				luxometer = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
			}
			if (luxometer != null) {
				sensorManager.registerListener(this, luxometer, SensorManager.SENSOR_DELAY_NORMAL);
			}
		} else {
			if (luxometer != null) {
				if (luxometer != null) sensorManager.unregisterListener(this, luxometer);
				luxometer = null;
			}
		}
		if (hasMag) {
			if (magnetometer == null) {
				magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
			}
			if (magnetometer != null) {
				sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
			}
		} else {
			if (magnetometer != null) {
				if (magnetometer != null) sensorManager.unregisterListener(this, magnetometer);
				magnetometer = null;
			}
		}
		if (hasAccel) {
			if (accelerometer == null) {
				accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			}
			if (accelerometer != null) {
				sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
			}
		} else {
			if (accelerometer != null) {
				if (accelerometer != null) sensorManager.unregisterListener(this, accelerometer);
				accelerometer = null;
			}
		}
		if (hasGyro) {
			if (gyro == null) {
				gyro = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			}
			if (gyro != null) {
				sensorManager.registerListener(this, gyro, SensorManager.SENSOR_DELAY_NORMAL);
			}
		} else {
			if (gyro != null) {
				if (gyro != null) sensorManager.unregisterListener(this, gyro);
				gyro = null;
			}
		}
		return true;
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1)
	{
	}

	@Override
	public void onSensorChanged(SensorEvent event)
	{
//		Log.d("sensor", String.format("roto %f %f %f", event.values[0], event.values[1], event.values[2]));
		Patch p = getCurrentPatch();
		if (p == null) {
			return;
		}
		if (event.sensor == accelerometer) {
		} else if (event.sensor == rotometer) {
			float [] rv = new float[3];
			for (int i=0; i<3; i++) {
				rv[i] = (event.values[i]+1)/2;
			}
			for (SXControlAssign sxp: p.sensorAssign) {
				switch(sxp.getSensorId()) {
				case SensorInfo.ROT_X: {
					sxp.setNormalizedValue(rv[0]);
					ccontrolUpdate(sxp.controllable, sxp.xp.ccontrolId(), sxp.x, Controllable.Controller.SENSOR);
					break;
				}

				case SensorInfo.ROT_Y: {
					sxp.setNormalizedValue(rv[1]);
					ccontrolUpdate(sxp.controllable, sxp.xp.ccontrolId(), sxp.x, Controllable.Controller.SENSOR);
					break;
				}

				case SensorInfo.ROT_Z: {
					sxp.setNormalizedValue(rv[2]);
					ccontrolUpdate(sxp.controllable, sxp.xp.ccontrolId(), sxp.x, Controllable.Controller.SENSOR);
					break;
				}		
				
				}
			}
		} else if (event.sensor == gyro) {
		} else if (event.sensor == proxometer) {
//			Log.d("sensor", "proximity "+event.values[0]);
		} else if (event.sensor == magnetometer) {
		} else if (event.sensor == luxometer) {
//			Log.d("sensor", "light "+event.values[0]);
		}
	}

	/***************************************
	 * ANIMATION LISTENER IMPLEMENTATIONS
	 ***************************************/
	/**
	 * @param animation
	 */
	public void onAnimationEnd(Animation animation)
	{
	}

	/**
	 * @param animation
	 */
	public void onAnimationRepeat(Animation animation)
	{
	}

	/**
	 * @param animation
	 */
	public void onAnimationStart(Animation animation)
	{
	}
	
	/***************************************
	 * EVENT HANDLING
	 ***************************************/
	protected void onPeriodicUiUpdate() {
		for (PadButton p: padButton) {
			if (p.getSample().isPlaying()) {
				p.invalidate(p.getPositionDisplayRect());
//				p.invalidate();
			}
		}
		if (sampleEditorFragment.isVisible() && selectedPadSample != null && selectedPadSample.isPlaying()) {
			sampleEditorFragment.updateSampleCursor();
		}
	}

	private void handleRecordClosed() {
		recButton.setText("Rec");
		recSample.setSamplePath(recFile.getAbsolutePath(), true);
		recPadButton.setRecordState(false);
		if (recSampleWasPlaying) {
			startPad(recPadButton);
		}

		recPadButton = null;
		recSample = null;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
//		Log.d("result", "got actity "+Integer.toString(requestCode));
		switch(requestCode) {
		
		case EventID.CHOOSE_FINDER: {
			if(resultCode == RESULT_OK) {
				String filePath = data.getExtras().getString("filePath");
				if (selectedPadSample != null && filePath != null) {
//					Log.d("result", "got actity and all ok "+Integer.toString(editPadButton.getId()));
					selectedPadSample.setSamplePath(filePath, true);
					if (sampleEditorFragment != null) sampleEditorFragment.setSampleGfx4Pad(selectedPadSample);
					File fp = new File(filePath);
					if (fp.getParent() != null) {
						lastSampleFolder = fp.getParent();
					}
				}
			}
			break;
		}
		
		case EventID.SANITIZE_FINDER: {
			if(resultCode == RESULT_OK) {
				String filePath = data.getExtras().getString("filePath");
				if (filePath != null) {
					BankSanitizer.fileBrowserResult(filePath);
				}
			}
			break;
		}
		
		case R.id.sg_menu_load_bank: {
			if(resultCode == RESULT_OK) {
				String filePath = data.getExtras().getString("filePath");
				if (filePath != null) {
					currentBankFile = new File(filePath);
					currentBank = loadBank(currentBankFile);
					setCurrentPatchInd(0);
					loadPatchDisplayUpdate();
				}
			}
			break;
		}
		
		case R.id.sg_menu_preferences: {
			doPreferences();
			break;
		}
		}
		
	}
	
	private void doPreferences()
	{
		sgPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		keepScreenAlive  = sgPreferences.getBoolean("screenKeepAlive",true);
		defaultSampleSearchPath = sgPreferences.getString("defaultSampleSearchPath",null);
		if (keepScreenAlive) {
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		} else {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	/***************************************
	 * GETTERS, SETTTERS, FORGETTERS
	 ***************************************/
	
	public void setTempo(float t)
	{
		for (PadSample ps: padSample) {
			ps.setTempo(t);
		}
		BusMixer.setTempo(t);
	}
	
	/***************************************
	 * PATCH AND BANK, STORE AND LOAD
	 ***************************************/
	@Override
	protected File getStorageBaseDir() {
		File f = new File(Environment.getExternalStorageDirectory()+"/"+"mayaswell", "spacegun");
		if (!f.exists()) {
			f.mkdirs();
			// also check noe and see if we should migrate
			File orig = getExternalFilesDir(null);
			Collection<File> existingContent = FileUtils.listFiles(orig, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			for (File oldFile: existingContent) {
				try {
					FileUtils.moveToDirectory(oldFile, f, false);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			MediaScannerConnection.scanFile(this, new String[] {f.toString()}, null, null);
		}
		return f;
	}

	protected void setPatch(Patch p)
	{
		int i = 0;
//		Log.d("setpatch", String.format("pads %d %d name %s", padSample.size(), p.padState.size(), p.name));
		if (p == null) {
			return;
		}
		for (i=0; i<padSample.size() && i<p.padState.size(); i++) {
			PadSample ps = padSample.get(i);
			PadSampleState pss = p.padState.get(i);
			if (pss == null) break;
			ps.setState(pss);
			patchTempoControl.setValue(p.tempo);
			if (selectedPadSample == ps) {
				setSubPanelsToPad(ps);
			}
		}
		busMixer.setBusStates(p.busState);
		if (sensorAssignFragment != null) {
			sensorAssignFragment.setToPatch(p);
		}
		setTempo(p.tempo);
		for (;i<padSample.size(); i++) {
			PadSample ps = padSample.get(i);
			ps.setState(new PadSampleState());
		}
		if (busControlsFragment != null) {
			busControlsFragment.refreshDisplayedBusState();
		}
		loadPatchDisplayUpdate();
	}

	/**
	 *   this just updates the display in the spinner
	 * @param cpInd
	 */
	protected void setPatchSelector(int cpInd)
	{
		if (cpInd < 0 || cpInd >= currentBank.numPatches()) {
			cpInd = 0;
		}
		for (int i=0; i<patchSelectAdapter.getCount(); i++) {
			Action p = patchSelectAdapter.getItem(i);
			if (p.getOp() == cpInd) {
				patchSelector.setSelection(i);
				break;
			}
		}
	}
	
	protected void setCurrentPatchState(Patch p)
	{
		if (p == null) return;
		int i = 0;
		for (i=0; i<padSample.size(); i++) {
			PadSample ps = padSample.get(i);
			if (i>=p.padState.size()) {
				p.padState.add(ps.state);
			} else {
				if (p.padState.get(i) != ps.state) {
					p.padState.remove(i);
					p.padState.add(i, ps.state);
				}
			}
		}
		for (i=0; i<busMixer.bus.size(); i++) {
			Log.d("set patch state", String.format("bus %d", i));
			Bus b = busMixer.bus.get(i);
			if (i>=p.busState.size()) {
				p.busState.add(b.state);
			} else {
				if (p.busState.get(i) != b.state) {
					p.busState.remove(i);
					p.busState.add(i, b.state);
				}
			}
		}
	}
	
	public Bank loadBank(String fnm, boolean internal, boolean qq)
	{
		FileInputStream fp;
		Bank b = null;
		try {
			if (internal) {
				fp = openFileInput(fnm);
				b = new Bank();
				b.load(fp);
				fp.close();
				b.setPatchAdapterItems(patchSelectAdapter);
				BankSanitizer.finish();
				if (automaticBankSanitize) {
					BankSanitizer.check(b, this, null, null);
				}
			} else {
			}
		} catch (FileNotFoundException e) {
			if (qq) {
				e.printStackTrace();
				mafFalog("File exception while loading bank "+e.getMessage());
			}
		} catch (IOException e) {
			if (qq) {
				e.printStackTrace();
				mafFalog("IO exception while loading bank "+e.getMessage());
			}
		}
		return b;
	}
	
	public Bank loadBank(File file)
	{
		FileInputStream fp;
		Bank b = currentBank;
		try {
			fp = new FileInputStream(file);
			b = new Bank();
			b.load(fp);
			b.setPatchAdapterItems(patchSelectAdapter);
			fp.close();
			if (bankSanitizer != null) {
				bankSanitizer.finish();
			}
			if (automaticBankSanitize) {
				bankSanitizer  = BankSanitizer.getInstance();
				BankSanitizer.check(b, this, null, null);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			mafFalog("File exception while loading bank "+e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			mafFalog("IO exception while loading bank "+e.getMessage());
		}
		return b;
	}
	
	private void loadPatchDisplayUpdate()
	{
		if (padButton != null) {
			for (PadButton pb: padButton) {
				pb.invalidate();
			}
		}
		if (ribbonController != null) {
			ribbonController.invalidate();
		}
		refreshTouchPad();
		if (selectedPadButton != null) {
			selectPad(selectedPadButton);
		}
		checkStartStopSensors();
	}

	protected boolean cleanVerifyCutAndRun(boolean andState)
	{
		if (currentBankFile != null) {
			saveCurrentBank(true, andState);
		} else {
			saveFialog("bank.sgx", null, true, andState);
		}
		return false;
	}

	protected void saveCurrentBank(final boolean andQuit, final boolean andPatch)
	{
		if (currentBankFile != null && currentBank != null) {
			if (andPatch && currentPatchInd >= 0 && currentPatchInd < currentBank.patch.size()) {
				setCurrentPatchState(currentBank.patch.get(currentPatchInd));
			}
			saveBank(currentBank, currentBankFile);
			if (andQuit) {
				cleanExit();
			}
		}
	}

	protected void cleanExit() {
		Log.d("SpaceGun", "calling it quits");
		finish();
	}



	/********************************************
	 * INTERFACE AND DIALOG HOOKS AND WRAPPERS
	 ********************************************/

	private void showHelp()
	{
		Builder d = new AlertDialog.Builder(this);
		if (fragmentHolder.isShowing(mainControlsFragment)) {
			d.setTitle(getResources().getString(R.string.help_title_pad_edit));
			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_pad_edit)));
		} else {
			d.setTitle(getResources().getString(R.string.help_title_main));
			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_main)));
		
//			d.setTitle(getResources().getString(R.string.help_title_general));
//			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_general)));
		}
		d.setPositiveButton(aboutButtonText(), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show(); 
	}

	private void showAbout()
	{
		PackageInfo pInfo;
		String versnm = "1.0.1";
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versnm = pInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		String appnm = getResources().getString(R.string.release_name);
//		String appniknm = getResources().getString(R.string.release_nickname);
		showTextDialog(appnm+", "+versnm, R.string.about_text);
	}

	private void showTextDialog(String title, int contentId)
	{
		int pbi = (int) Math.floor(Math.random()*aboutButtonText.length);
		Builder d = new AlertDialog.Builder(this);
		d.setTitle(title);
		d.setMessage(Html.fromHtml(getResources().getString(contentId)));
		d.setPositiveButton(aboutButtonText[pbi], new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show();
	}

	protected boolean startPad(final PadButton pb)
	{
		final PadSample p = pb.getSample();
		if (p.shouldStartPaused()) {
			p.firePaused();
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					pb.setPausedState(p.isPaused());
				}
			});
			return false;
		} else {
			Log.d("starting pad", String.format("%d", p.id));
			player.startPad(p);
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					pb.setPlayState(p.isPlaying());
				}
			});
			return true;
		}
	}
	
	protected boolean stopPad(final PadButton pb)
	{
		player.stopPad(pb.getSample());
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				pb.setPlayState(false);
			}
		});
		pb.getSample().stop(false);
		return true;
	}

	protected boolean startRecording(PadButton pb)
	{
		if (pb == null) {
			return false;
		}
		if (recPadButton != null) {
			// stopping recording must wait till the record thread finished ... we can't call it here
			//stopRecording();
			return false;
		}
//		Log.d("record", String.format("sample record started"));
		
		recPadButton = pb;
		recSample = pb.getSample();
		recSampleWasPlaying = recSample.isPlaying();
		stopPad(recPadButton);
		recPadButton.setRecordState(true);

		recButton.setText("Stop");

		/* TODO maybe put this fucking around in the stop ... or maybe leave it here ?? */
		if (currentBankFile == null) {
			File appDir = getStorageBaseDir();
			File recDir = new File(appDir, "Recorded");
			if (!recDir.exists()) {
				recDir.mkdir();
				lastRecFileNumber = 1;
			}
			recFile = getUniqueFile(recDir, "sample", lastRecFileNumber, player.saveFileExtension());
		} else {
			File bankDir = currentBankFile.getParentFile();
			String bankName = currentBankFile.getName();
			if (bankName.lastIndexOf('.')>0) {
				bankName = bankName.substring(0, bankName.lastIndexOf('.'));
			}
			File recDir = new File(bankDir, "Recorded_"+bankName);
			if (!recDir.exists()) {
				recDir.mkdir();
				lastRecFileNumber = 1;
			}
			recFile = getUniqueFile(recDir, "sample", lastRecFileNumber, player.saveFileExtension());
		}

		player.startRecording(recFile, new Observer<AudioFile> () {

			@Override
			public void onCompleted() {
				handleRecordClosed();
			}

			@Override
			public void onError(Throwable e) {
				handleErrorMessage(e.getMessage(), ErrorLevel.ERROR_EVENT);
			}

			@Override
			public void onNext(AudioFile audioFile) {

			}
		});
		return true;
	}
	
	private File getUniqueFile(File dir, String name, int tryno, String extension)
	{
		for (;;) {
			File file = new File(dir, name+Integer.toString(tryno)+"."+extension);
			if (!file.exists()) {
				lastRecFileNumber = tryno;
				return file;
			}
			tryno++;
		}
	}

	protected void stopRecording()
	{
//		Log.d("record", String.format("sample record stopped"));
		player.stopRecording();
	}

	public void selectPad(PadButton pb)
	{
//		Log.d("edit", String.format("start edit"));
		shouldPrimeEditButton = false;
		if (saveSelectButton != null) saveSelectButton.setPrimedState(false);
		if (pb == null) {
			return;
		}
		if (player.isRecording() && recPadButton == pb) {
			return;
		}
		PadSample pbs = pb.getSample();
		if (pbs == null) return;
		
		setHiglightToPad(pb);
		
		selectedPadButton = pb;
		selectedPadSample = pbs;
		
		setSubPanelsToPad(pbs);
		
	}

	private void setHiglightToPad(PadButton pb) {
		MarginLayoutParams rlp = (MarginLayoutParams) buttonSelectHilight.getLayoutParams();
		RelativeLayout.LayoutParams slp = new RelativeLayout.LayoutParams(rlp);
		slp.addRule(RelativeLayout.ALIGN_TOP, pb.getId());
		slp.addRule(RelativeLayout.ALIGN_LEFT, pb.getId());
		buttonSelectHilight.setLayoutParams(slp);
	}

	private void setSubPanelsToPad(PadSample pbs) {
		if (sampleEditorFragment != null) {
			sampleEditorFragment.setToPad(pbs);
		}
		if (mainControlsFragment != null) {
			mainControlsFragment.setToPad(pbs);
		}
		if (modControlsFragment != null) {
			modControlsFragment.setEnvHost(pbs);
			modControlsFragment.setLFOHost(pbs);
		}
		if (busControlsFragment != null) {
			busControlsFragment.setToPad(pbs);
		}
	}

	public void startCtrlEdit()
	{
		shouldPrimeEditButton = false;
		if (saveSelectButton != null) saveSelectButton.setPrimedState(false);
		Patch p = getCurrentPatch();
		if (p != null && sensorAssignFragment != null) {
			fragmentHolder.showFragment(sensorAssignFragment);
			sensorAssignFragment.setToPatch(p);
		}
	}

	public void startTouchpadEdit()
	{
		startCtrlEdit();
	}
	
	public void startRibbonEdit()
	{
		startCtrlEdit();
	}
	
	public Controllable findControllable(String dt, int di)
	{
		Log.d("spacegun", String.format("controllable %s %d", dt, di));
		if (dt.equals("pad")) {
			for (PadSample ps: padSample) {
				if (ps != null && ps.id == di) {
					return ps;
				}
			}
			if (di <= padSample.size() && di > 0) {
				return padSample.get(di-1);
			}
		} else if (dt.equals("bus")) {
			for (Bus ps: busMixer.bus) {
				if (ps != null && ps.getBusId() == di) {
					return ps;
				}
			}
			if (di <= busMixer.bus.size() && di > 0) {
				return busMixer.bus.get(di-1);
			}
		}
		return null;
	}

	/*
	public static CControl findControl(Controllable c, String xt, int xi)
	{
		ControlsAdapter caa = c.getInterfaceTgtAdapter();
		for (int i=0; i<caa.getCount(); i++) {
			Control cc = caa.getItem(i);
			CControl ci = (CControl) caa.getItem(i);
			Log.d("Space gun", ci.ccontrolName());
			if (ci.ccontrolName().equals(xt)) {
				return ci;
			}
		}
		return null;
	}*/

	public SensorInfo findSensor(String st, int si)
	{
		for (int i=0; i<sensorAdapter.getCount(); i++) {
			SensorInfo sinf = sensorAdapter.getItem(i);
			if (sinf.getTag().equals(st)) {
				return sinf;
			}
		}
		return null;
	}

	public float getTempo()
	{
		Patch p;
		if ((p = getCurrentPatch()) != null) {
			return p.tempo;
		}
		return 120;
	}
	
	public void setShowSampleTimeBeats(boolean b)
	{
		showSampleTimeBeats = b;
		if (sampleEditorFragment != null) sampleEditorFragment.setBeatMode(b);
	}
	
	public boolean getShowSampleTimeBeats()
	{
		return showSampleTimeBeats;
	}

	public void refreshTouchPad() {
		if (touchBayFragment != null) touchBayFragment.checkRedraw();
	}

	/**
	 *  handle cross fragment control changes
	 * @param c
	 * @param ccontrolId
	 * @param changedBy
	 */
	@Override
	public void ccontrolUpdate(Controllable c, int ccontrolId, float v, int changedBy) {
//		Log.d("param update", String.format("%s cc %d v %g by %d", c.abbrevName(), ccontrolId, v, changedBy));
		if (changedBy != Controllable.Controller.INTERFACE) {
			if (c.controllableType() == Controllable.PAD) {
				PadSample p = (PadSample) c;
				PadSampleState ps = p.getState();
				if (p == selectedPadSample && ps != null) {
					switch (ccontrolId) {
					case CControl.GAIN: if (mainControlsFragment != null) mainControlsFragment.setGain(v); break;
					case CControl.PAN: if (mainControlsFragment != null) mainControlsFragment.setPan(v); break;
					case CControl.LOOP_START: if (sampleEditorFragment != null) sampleEditorFragment.setLoopStart((long) (v*p.nTotalFrames)); break;
					case CControl.LOOP_LENGTH: if (sampleEditorFragment != null) sampleEditorFragment.setLoopLength((long) (v*p.nTotalFrames)); break;
					case CControl.FLT_CUTOFF: if (mainControlsFragment != null) mainControlsFragment.setCutoff(v); break;
					case CControl.FLT_ENVMOD: if (mainControlsFragment != null) mainControlsFragment.setEnvMod(v); break;
					case CControl.FLT_RESONANCE: if (mainControlsFragment != null) mainControlsFragment.setResonance(v); break;
					case CControl.TUNE: if (mainControlsFragment != null) mainControlsFragment.setTune(v); break;
					default: {
						if (ccontrolId >= CControl.envIdBase(0) && ccontrolId < CControl.envIdBase(SpaceGun.Global.ENV_PER_PAD)) {
							int env = CControl.env4Id(ccontrolId);
							int param = CControl.envParam4Id(ccontrolId);
							if (env<ps.envelope.size()) {
								if (param == 0) { if (modControlsFragment != null) modControlsFragment.setAttackT(env, v); }
								else if (param == 1) { if (modControlsFragment != null) modControlsFragment.setDecayT(env, v); }
								else if (param == 2) {/* sustain */ if (modControlsFragment != null) modControlsFragment.setSustainT(env, v); } 
								else if (param == 3) {/* sustain level */ if (modControlsFragment != null) modControlsFragment.setSustainL(env, v); } 
								else if (param == 4) { /* release */ if (modControlsFragment != null) modControlsFragment.setReleaseT(env, v); }
								else {/* depth(param-5); */
									if ((param-5)<ps.envelope.get(env).target.size()) { 
										if (modControlsFragment != null) modControlsFragment.setLFOTargetGain(env, param-5, v); 
									}
								}
							}
						} else if (ccontrolId >= CControl.lfoIdBase(0) && ccontrolId < CControl.lfoIdBase(SpaceGun.Global.LFO_PER_PAD)) {
							int lfo = CControl.lfo4Id(ccontrolId);
							int param = CControl.lfoParam4Id(ccontrolId);
							if (lfo<ps.lfo.size()) {
								if (param == 0) { /* rate */if (modControlsFragment != null) modControlsFragment.setRate(lfo, v); }
								else if (param == 1) { /* phase */if (modControlsFragment != null) modControlsFragment.setPhase(lfo, v); } 
								else {/* depth(param-2);*/
									if ((param-2) < ps.lfo.get(lfo).target.size()) { 
										if (modControlsFragment != null) modControlsFragment.setEnvTargetGain(lfo, param-2, v);
									}
								}
							}
						} else if (ccontrolId >= CControl.busIdBase(0) && ccontrolId < CControl.busIdBase(SpaceGun.Global.MAX_BUS)) {
							int bus = CControl.bus4SendId(ccontrolId);
							int param = CControl.busParam4Id(ccontrolId);
							if (param == 0)	{
								if (busControlsFragment != null) {
									busControlsFragment.setSendGain(bus, v);
								}
							}
						}
					}
					}
				}
			} else if (c.controllableType() == Controllable.BUS) {
				Bus b = (Bus) c;
				BusState bs = b.getState();
				if (ccontrolId >= CControl.fxIdBase(0) && ccontrolId < CControl.fxIdBase(SpaceGun.Global.MAX_BUS_FX)) {
					int fx = CControl.fx4Id(ccontrolId);
					int param = CControl.fxParam4Id(ccontrolId);
					if (param == 0) { // enable
					} else {// param(pj-1);
						if (fx<bs.fx.size() && bs.fx.get(fx).getParams() != null && (param-1) < bs.fx.get(fx).getParams().length) { 
							if (busControlsFragment != null) busControlsFragment.setFXParam(fx, param-1, v);bs.fx.get(fx).param(param-1, v);
						}
					}
				} else if (ccontrolId >= CControl.lfoIdBase(0) && ccontrolId < CControl.lfoIdBase(SpaceGun.Global.LFO_PER_PAD)) {
					int lfo = CControl.lfo4Id(ccontrolId);
					int param = CControl.lfoParam4Id(ccontrolId);
					if (lfo<bs.lfo.size()) {
						if (param == 0) { /* rate */if (busControlsFragment != null) busControlsFragment.setRate(lfo, v); }
						else if (param == 1) { /* phase */if (busControlsFragment != null) busControlsFragment.setPhase(lfo, v); } 
						else {/* depth(param-2);*/
							if ((param-2) < bs.lfo.get(lfo).target.size()) { 
								if (busControlsFragment != null) busControlsFragment.setTargetGain(lfo, param-2, v);
							}
						}
					}
				}
			}
		}
		if (changedBy != Controllable.Controller.TOUCHPAD) {
			Patch p = currentPatch;
			if (p != null && touchBayFragment != null) {
				for (XYControlAssign xyp: p.touchAssign) {
					if (xyp != null && c == xyp.controllable) {
						if (ccontrolId == xyp.xp.ccontrolId()) {
							touchBayFragment.updateDisplayX(xyp, v);
						} else if (ccontrolId == xyp.yp.ccontrolId()) {
							touchBayFragment.updateDisplayY(xyp, v);
						}
					}
				}
			}
		}
	}

	public PadSample padForId(int id) {
		for (PadSample p: padSample) {
			if (p.id == id) {
				return p;
			}
		}
		return null;
	}

	@Override
	public Context getContext() {
		return this;
	}

	@Override
	public int countSamplePlayers() {
		return padSample != null? padSample.size(): 0;
	}

	@Override
	public SamplePlayer getSamplePlayer(int i) {
		return padSample != null && i < padSample.size() && i >= 0 ? padSample.get(i): null;
	}

	@Override
	public void startSamplePlayer(SamplePlayer p, boolean fromSync) {
		PadSample ps;
		if ((ps=(PadSample)p) != null) {
			player.startPad(ps, fromSync);
		}
	}

	@Override
	public void stopSamplePlayer(SamplePlayer p) {
		PadSample ps;
		if ((ps=(PadSample)p) != null) {
			player.stopPad(ps);
			final PadButton pb = (PadButton) ps.padButton;
			runOnUiThread( new Runnable() {
				@Override
				public void run() {
					pb.setPlayState(false);
				}
			});
			pb.getSample().stop(false);
		}
	}

	public void invalidateTouchPad(XYControlAssign xyAssgn) {
		if (touchBayFragment != null && touchBayFragment.touchPad != null) {
			touchBayFragment.touchPad.invalidate();
		}
	}

	@Override
	public int lfoRateId(int xyci) {
		return CControl.lfoRateId(xyci);
	}

	@Override
	public int lfoPhaseId(int xyci) {
		return CControl.lfoPhaseId(xyci);
	}

	@Override
	public int lfoTargetDepthId(int xyci, int tid) {
		return CControl.lfoTargetDepthId(xyci, tid);
	}

	@Override
	public int envAttackTimeId(int xyci) {
		return CControl.envAttackTimeId(xyci);
	}

	@Override
	public int envDecayTimeId(int xyci) {
		return CControl.envDecayTimeId(xyci);
	}

	@Override
	public int envReleaseTimeId(int xyci) {
		return CControl.envReleaseTimeId(xyci);
	}

	@Override
	public int envSustainTimeId(int xyci) {
		return CControl.envSustainTimeId(xyci);
	}

	@Override
	public int envSustainLevelId(int xyci) {
		return CControl.envSustainLevelId(xyci);
	}

	@Override
	public int envTargetDepthId(int xyci, int tid) {
		return CControl.envTargetDepthId(xyci, tid);
	}

	@Override
	public ArrayAdapter<ResetMode> getLfoRstModeAdapter(LFO.Host h) {
		return PadSample.lfoRstModeAdapter;
	}

	@Override
	public ArrayAdapter<ResetMode> getEnvRstModeAdapter(Envelope.Host h) {
		return PadSample.envRstModeAdapter;
	}

	@Override
	public ArrayAdapter<LFWave> getLfoWaveAdapter(LFO.Host h) {
		return PadSample.lfoWaveAdapter;
	}

	@Override
	public Patch clonePatch(Patch p) {
		return p != null? p.clone():null;
	}

	@Override
	public ArrayAdapter<FX> getFXAdapter(Host h) {
		return fxAdapter;
	}
	
	@Override
	public boolean alwaysTempoLock()
	{
		return false;
	}

	@Override
	public ArrayAdapter<ControllableInfo> getControllableAdapter() {
		return controllableAdapter;
	}

/*
 * mwbppadactivity<?,?,?> overrides 
 * 
 */
	@Override
	public PadSample getPadSample(int i) {
		return (i >= 0 && i<padSample.size())? padSample.get(i): null;
	}

	@Override
	public float getSampleRate() {
		return SGAudioMixer.dfltSampleRate;
	}

	@Override
	public int countPadSamples() {
		return padSample.size();
	}

	@Override
	public PadSample getSelectedPad() {
		return selectedPadSample;
	}

	@Override
	public ArrayAdapter<MenuData<PadMode>> getPadModeAdapter() {
		return padModeAdapter;
	}
	
	@Override
	public BusMixer getBusMixer() {
		return busMixer;
	}


}