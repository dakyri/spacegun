package com.mayaswell.spacegun;

import java.io.File;
import java.util.ArrayList;

import android.util.Log;
import android.widget.ArrayAdapter;

import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Controllable.RXControlAssign;
import com.mayaswell.audio.Controllable.SXControlAssign;
import com.mayaswell.audio.Controllable.XControlAssign;
import com.mayaswell.audio.Controllable.XYControlAssign;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.spacegun.CControl;
import com.mayaswell.util.AbstractPatch;

public class Patch extends AbstractPatch
{
	public float tempo = 120;
	public ArrayList<PadSampleState> padState = null;
	public ArrayList<BusState> busState = null;
	public ArrayList<XYControlAssign> touchAssign=null;
	public ArrayList<SXControlAssign> sensorAssign=null;
	public ArrayList<RXControlAssign> ribbonAssign=null;

	public Patch()
	{
		name = "";
		padState = new ArrayList<PadSampleState>();
		busState = new ArrayList<BusState>();
		touchAssign = new ArrayList<XYControlAssign>();
		sensorAssign = new ArrayList<SXControlAssign>();
		ribbonAssign = new ArrayList<RXControlAssign>();
	}
	
	public Patch(String nm, boolean init)
	{
		name = new String(nm);
		padState = new ArrayList<PadSampleState>();
		busState = new ArrayList<BusState>();
		touchAssign = new ArrayList<XYControlAssign>();
		sensorAssign = new ArrayList<SXControlAssign>();
		ribbonAssign = new ArrayList<RXControlAssign>();
		if (init) {
			add(new PadSampleState());
			add(new PadSampleState());
			add(new PadSampleState());
			padState.get(0).pan = -0.8f;
			padState.get(2).pan = 0.8f;
			Controllable c = null;
			ControlsAdapter ca = null;
			if ((c=SpaceGun.sg.findControllable("pad", 1)) != null && (ca = c.getInterfaceTgtAdapter()) != null) {
				touchAssign.add(new XYControlAssign(c, ca.findControl("pan", 0), ca.findControl("gain", 0)));
			}
			if ((c=SpaceGun.sg.findControllable("pad", 2)) != null && (ca = c.getInterfaceTgtAdapter()) != null) {
				touchAssign.add(new XYControlAssign(c, ca.findControl("pan", 0), ca.findControl("gain", 0)));
			}
			if ((c=SpaceGun.sg.findControllable("pad", 3)) != null && (ca = c.getInterfaceTgtAdapter()) != null) {
				touchAssign.add(new XYControlAssign(c, ca.findControl("pan", 0), ca.findControl("gain", 0)));
			}
			if ((c=SpaceGun.sg.findControllable("pad", 4)) != null && (ca = c.getInterfaceTgtAdapter()) != null) {
				RXControlAssign rx = new RXControlAssign(c, ca.findControl("gain", 0));
				ribbonAssign.add(rx);
			}
			if ((c=SpaceGun.sg.findControllable("pad", 4)) != null && (ca = c.getInterfaceTgtAdapter()) != null) {
				RXControlAssign rx = new RXControlAssign(c, ca.findControl("gain", 0));
				rx.setMapParams(1f, 0.5f, 0f); // reverse it, so the ribbon is an xfade
				ribbonAssign.add(rx);
			}
		}
	}
	
	public Patch clone()
	{
		Patch p = new Patch(name, false);
		for (PadSampleState pss: padState) {
			if (pss != null) {
				p.padState.add(pss.clone());
			}
		}
		for (BusState pss: busState) {
			if (pss != null) {
				p.busState.add(pss.clone());
			}
		}
		for (XYControlAssign pss: touchAssign) {
			if (pss != null) {
				p.touchAssign.add(pss.clone());
			}
		}
		for (SXControlAssign pss: sensorAssign) {
			if (pss != null) {
				p.sensorAssign.add(pss.clone());
			}
		}
		for (RXControlAssign pss: ribbonAssign) {
			if (pss != null) {
				p.ribbonAssign.add(pss.clone());
			}
		}
		return p;
	}
	
	public boolean add(PadSampleState pss)
	{
		return padState.add(pss);
	}
	
	public boolean add(BusState pss)
	{
		return busState.add(pss);
	}


	public boolean add(XYControlAssign xss)
	{
		if (xss == null) return false;
		/*
		if (xss.controllerType != null && xss.controllerId > 0) {
			if (xss.controllerType.equals("touch")) {
				for (int i= touchAssign.size(); i<xss.controllerId-1; i++) {
					touchAssign.add(i, null);
				}
				touchAssign.add(xss.controllerId-1, xss);
				return true;
			}
		} else {
			touchAssign.add(xss);
			return true;
		}*/
		touchAssign.add(xss);
		return false;
	}
	
	public boolean add(SXControlAssign xss)
	{
		if (xss == null) return false;
		sensorAssign.add(xss);
		return true;
	}
	
	public boolean add(RXControlAssign xss)
	{
		if (xss == null) return false;
		ribbonAssign.add(xss);
		return true;
	}

	public void setTempo(float v)
	{
		tempo = v;
	}

	
	public boolean getMissingFiles(ArrayList<String> fl) {
		boolean b = false;
		for (PadSampleState pss: padState) {
			if (pss != null && pss.path != null && !pss.path.equals("")) {
				File f = new File(pss.path);
				if (!f.exists()) {
					if (!fl.contains(pss.path)) {
					 fl.add(pss.path);
					}
					b = true;
				}
			}
		}
		return b;
	}
	
	public boolean fixFilePath(String oldPath, String newPath) {
		boolean b = false;
		for (PadSampleState pss: padState) {
			if (pss != null && pss.path != null) {
				if (pss.path.equals(oldPath)) {
					pss.path = newPath;
					b = true;
				}
			}
		}
		return b;
	}

}
