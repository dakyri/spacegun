package com.mayaswell.spacegun.widget;

import com.mayaswell.widget.BaseControl;

import android.content.Context;
import android.util.AttributeSet;

public class OSCControl extends BaseControl {
	public OSCControl(Context context)
	{
		super(context);
		setup(context, null);
	}

	public OSCControl(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	public OSCControl(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setup(context, attrs);
	}

	protected void setup(Context context, AttributeSet attrs)
	{
		
	}
}
