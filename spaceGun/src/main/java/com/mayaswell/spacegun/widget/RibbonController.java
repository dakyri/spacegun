package com.mayaswell.spacegun.widget;

import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Controllable.RXControlAssign;
import com.mayaswell.spacegun.Patch;
import com.mayaswell.spacegun.R;
import com.mayaswell.spacegun.SpaceGun;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class RibbonController extends View
{
	final float densityMultiplier = getContext().getResources().getDisplayMetrics().density;

	private boolean showGrid = true;
	private int nGridX = 10;
	
	private Paint textBrush = null;
	private Paint gridBrush = null;
	private Paint pointBrush = null;
	private int padW;
	private int padH;
	
	private int[] colors = {
		getResources().getColor(R.color.ribbonBow1),
		getResources().getColor(R.color.ribbonBow2),
		getResources().getColor(R.color.ribbonBow3),
		getResources().getColor(R.color.ribbonBow4),
		getResources().getColor(R.color.ribbonBow5),
		getResources().getColor(R.color.ribbonBow6),
		getResources().getColor(R.color.ribbonBow7),
		getResources().getColor(R.color.ribbonBow8)
	};

	final float textScaledPx = 10 * densityMultiplier;
	
	public RibbonController(Context context) {
		super(context);
		setup(context, null, 0);
	}

	public RibbonController(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, 0);
	}

	public RibbonController(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		padW = getWidth(); 
		padH = getHeight();
		
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setColor(0xdd4285F4);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.CENTER);
		
		gridBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		gridBrush.setColor(0x33888888);
		gridBrush.setStrokeWidth(2);

		pointBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		pointBrush.setColor(0xff111111);
	}

	private float px2X(float px)
	{
		if (padH == 0) return 0;
		float pnx = 1-((float)px)/padH;
		if (pnx < 0) pnx = 0; else if (pnx > 1) pnx = 1;
		return pnx; 
	}
	
	private float x2px(float x)
	{
		if (x < 0) x = 0; else if (x > 1) x = 1;
		return padH*(1-x);
	}
	
	@Override
	protected void onDraw (Canvas canvas)
	{
		padW = canvas.getWidth();
		padH = canvas.getHeight();
		
		Patch p = null;
		if (SpaceGun.sg != null) p = SpaceGun.sg.getCurrentPatch();
		float cntX = padW/2;
		float sepW = 4;
		if (p != null) {
			int assgnInd = 0;
			int colorInd = 0;
			for (RXControlAssign xyp: p.ribbonAssign) {
				if (xyp != null && xyp.isValid()) {
					gridBrush.setColor(colors[colorInd]);
					if (++colorInd >= colors.length) colorInd = 0;
					float gridX = ((assgnInd % 2) == 1? cntX-sepW*assgnInd:cntX+sepW*assgnInd);
					canvas.drawLine(gridX, x2px(xyp.rangeMin), gridX, x2px(xyp.rangeMax), gridBrush);
					
					assgnInd++;
				}
				
			}
			colorInd = assgnInd = 0;
			float txtPos[] = new float[p.ribbonAssign.size()];
			for (RXControlAssign xyp: p.ribbonAssign) {
				if (xyp != null && xyp.isValid()) {
					String xls = xyp.abbrevLabelString();
					
					textBrush.setColor(colors[colorInd]);
					if (++colorInd >= colors.length) colorInd = 0;
					
					float maxValY = xyp.mapCenter;
					float maxValR = xyp.rangeCenter;
					float txtOff = (textBrush.descent()-textBrush.ascent())/2;
					if (xyp.mapMax > maxValY) {
						maxValY = xyp.mapMax;
						maxValR = xyp.rangeMax;
						txtOff = (textBrush.descent()-textBrush.ascent());
					}
					else if (xyp.mapMin > maxValY) {
						maxValY = xyp.mapMin;
						maxValR = xyp.rangeMin;
						txtOff = -textBrush.descent();
					}
									
//					float gridX = ((assgnInd % 2) == 1? cntX-sepW*assgnInd:cntX+sepW*assgnInd);
					float labelY = x2px(maxValR) + txtOff;
					float tryY = labelY;
					int pos = assgnInd;
					float fontSpace = textBrush.getFontSpacing();
					for (int k=0; k<assgnInd; k++) {
						float f = txtPos[k];
						if (tryY > padH/2) {
							if (tryY <= f && tryY + fontSpace >=f) {
								tryY -= fontSpace;
							}
						} else {
							if (tryY >= f && tryY-fontSpace <= f) {
								tryY += fontSpace;
							}
						}
						if (tryY < f) {
							pos = k;
						}
					}
					labelY = tryY;
					txtPos[pos] = tryY;
					if (labelY < 0) {
					//	labelY = 0;
					}else if (labelY > padH) {
					//	labelY = padH;				
					}
					canvas.drawText(xls, cntX, labelY, textBrush);
					
					assgnInd++;
				}
			}
		}
	}
	
	@Override
	public boolean onTouchEvent (MotionEvent e)
	{
		if (SpaceGun.sg != null && SpaceGun.sg.saveSelectButton != null && (SpaceGun.sg.saveSelectButton.isPressed() || SpaceGun.sg.saveSelectButton.getPrimedState())) {
			SpaceGun.sg.startRibbonEdit();
			return false;
		}
		int action = e.getActionMasked();

		Patch p = SpaceGun.sg.getCurrentPatch();
		if (p == null) {
			return false;
		}		
		
		float pnx = px2X(e.getY());
		
//		Log.d("ribbon", String.format("touch %f %f %d %d", pnx, e.getY(), padH, p.ribbonAssign.size()));
		
		if (action == MotionEvent.ACTION_UP) {
		} else {
		}
		for (RXControlAssign sxp: p.ribbonAssign) {
			sxp.setNormalizedValue(pnx);
			SpaceGun.sg.ccontrolUpdate(sxp.controllable, sxp.xp.ccontrolId(), sxp.x, Controllable.Controller.RIBBON);
		}
		return true;
	}
}
