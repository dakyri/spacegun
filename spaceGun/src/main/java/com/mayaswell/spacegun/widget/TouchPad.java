package com.mayaswell.spacegun.widget;

import java.util.ArrayList;

import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Controllable.XYControlAssign;
import com.mayaswell.spacegun.Patch;
import com.mayaswell.spacegun.SpaceGun;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;

public class TouchPad extends View
{
	final float densityMultiplier = getContext().getResources().getDisplayMetrics().density;

	private boolean showGrid = true;
	private int nGridX = 10;
	private int nGridY = 10;
	
	private Paint textBrush = null;
	private Paint gridBrush = null;
	private Paint pointBrush = null;
	private int padW;
	private int padH;
	
	final float textScaledPx = 10 * densityMultiplier;
	
	public TouchPad(Context context) {
		super(context);
		setup(context, null);
	}

	public TouchPad(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs);
	}

	public TouchPad(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs);
	}
	
	private void setup(Context context, AttributeSet attrs)
	{
		padW = getWidth(); 
		padH = getHeight();
		
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setColor(0xdd4285F4);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.CENTER);
		
		gridBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		gridBrush.setColor(0x33888888);

		pointBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		pointBrush.setColor(0xff111111);
	}

	protected void onDraw (Canvas canvas)
	{
/** TODO still not sure if these should be view width or canvas width */
		padW = canvas.getWidth();
		padH = canvas.getHeight();

		float geW = ((float)padW) / nGridX;
		float geH = ((float)padH) / nGridY;
		float gridX = geW;
		float gridY = geH;
		for (int i=0; i<nGridX-1; i++) {
			canvas.drawLine(gridX, 0, gridX, padH, gridBrush);
			gridX += geW;
		}
		for (int i=0; i<nGridY-1; i++) {
			canvas.drawLine(0, gridY, padW, gridY, gridBrush);
			gridY += geH;
		}
		int cl = 15;
		Patch p = null;
		if (SpaceGun.sg != null) p = SpaceGun.sg.getCurrentPatch();
		if (p != null) {
			for (XYControlAssign xyp: p.touchAssign) {
				if (xyp != null && xyp.isValid()) {
					xyp.refreshNormalizedValue();
					float dnx = xyp.nx*padW;
					float dny = (1-xyp.ny)*padH;
					canvas.drawLine(dnx-cl, dny, dnx+cl, dny, pointBrush);
					canvas.drawLine(dnx, dny-cl, dnx, dny+cl, pointBrush);
					String xls = xyp.labelString();
					String xvs = xyp.valueString();
//					Log.d("touch", xls+" "+xvs);
					float xlsl = textBrush.measureText(xls)/2;
					float xvsl = textBrush.measureText(xvs)/2;
					float mlvsl = (xlsl>xvsl?xlsl:xvsl);
					
					xyp.l = (int) (dnx - xlsl);
					xyp.r = (int) (dnx + xlsl);
					xyp.t = (int) (dny - xlsl);
					xyp.b = (int) (dny + xvsl);

					float ylsl = textBrush.descent()-textBrush.ascent();
					float yvsl = textBrush.getFontSpacing();
					
					if (dnx-mlvsl<0)
						dnx = mlvsl;
					else if (dnx+mlvsl>padW)
						dnx = padW-mlvsl;
					if (dny-ylsl < 0)
						dny = ylsl;
					else if (dny + yvsl > padH)
						dny = padH - yvsl;
					canvas.drawText(xls, dnx, dny-textBrush.descent(), textBrush);
					dny += textBrush.getFontSpacing();
					canvas.drawText(xvs, dnx, dny, textBrush);
				}
			}
		}
	}
	
	@Override
	protected void onSizeChanged (int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	@Override
	public boolean onTouchEvent (MotionEvent e)
	{
		if (SpaceGun.sg != null && SpaceGun.sg.saveSelectButton != null && (SpaceGun.sg.saveSelectButton.isPressed() || SpaceGun.sg.saveSelectButton.getPrimedState())) {
			SpaceGun.sg.startTouchpadEdit();
			clearXYCATrack(null);
			return false;
		}
		int action = e.getActionMasked();
		ArrayList<XYControlAssign> tracked = new ArrayList<XYControlAssign>();
		for (int i=0; i<e.getPointerCount(); i++) {
			float x = e.getX(i);
			float y = e.getY(i);
			float pnx = x/padW;
			float pny = 1-(y/padH);
			if (pnx < 0) pnx = 0; else if (pnx > 1) pnx = 1;
			if (pny < 0) pny = 0; else if (pny > 1) pny = 1;

			XYControlAssign xyp = findXYCA(e.getPointerId(i), pnx, pny);
			if (xyp != null) {
				invalidate(xyp.l, xyp.t, xyp.r, xyp.b);
				xyp.setNormalizedValue(pnx, pny);
				SpaceGun.sg.ccontrolUpdate(xyp.controllable, xyp.xp.ccontrolId(), xyp.x, Controllable.Controller.TOUCHPAD);
				SpaceGun.sg.ccontrolUpdate(xyp.controllable, xyp.yp.ccontrolId(), xyp.y, Controllable.Controller.TOUCHPAD);
				tracked.add(xyp);
			}
		}

		if (action == MotionEvent.ACTION_UP) {
			clearXYCATrack(null);
		} else {
			clearXYCATrack(tracked);
		}
//		invalidate();
		return true;
	}
	
	private void clearXYCATrack(ArrayList<XYControlAssign> tracked)
	{
		Patch p = SpaceGun.sg.getCurrentPatch();
		if (p != null) {
			for (XYControlAssign xyp: p.touchAssign) {
				if (xyp != null && (tracked == null || !tracked.contains(xyp))) {
					xyp.trackId = -1;
				}
			}
		}
	}

	private XYControlAssign findXYCA(int pointerId, float pnx, float pny)
	{
		Patch p = SpaceGun.sg.getCurrentPatch();
		if (p != null) {
			XYControlAssign nxyp = null;
			float nxypd = Float.MAX_VALUE;
			for (XYControlAssign xyp: p.touchAssign) {
				if (xyp != null && xyp.isValid() && xyp.trackId == pointerId) {
					return xyp;
				}
			}
			for (XYControlAssign xyp: p.touchAssign) {
				if (xyp != null && xyp.isValid() && xyp.trackId < 0) {
					float d = dxy(xyp.nx-pnx, xyp.ny-pny);
					if (d < nxypd) {
						nxypd = d;
						nxyp = xyp;
					}
				}
			}
			if (nxyp != null) {
				nxyp.trackId = pointerId;
				return nxyp;
			}
		}
		return null;
	}

	private float dxy(float xd, float yd) {
		return (float) Math.sqrt(xd*xd+yd*yd);
	}

	@Override
	public boolean onDragEvent (DragEvent event)
	{
		return super.onDragEvent(event);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);
//		Log.d("touch", String.format("onMeaure %s %s", View.MeasureSpec.toString(widthMeasureSpec), View.MeasureSpec.toString(heightMeasureSpec)));
		int h = heightSize;
		int w = widthSize;
//		if (h > w) {
//			h = w;
//		} else if (w > h) {
//			w = h;
//		}
		setMeasuredDimension(w, h);
	}

	private void updateXYPosRect(XYControlAssign xyp) {
		float dnx = xyp.nx*padW;
		float dny = (1-xyp.ny)*padH;
		String xls = xyp.labelString();
		String xvs = xyp.valueString();
		float xlsl = textBrush.measureText(xls)/2;
		float xvsl = textBrush.measureText(xvs)/2;
		xyp.l = (int) (dnx - xlsl);
		xyp.r = (int) (dnx + xlsl);
		xyp.t = (int) (dny - xlsl);
		xyp.b = (int) (dny + xvsl);
	}

	public void updateDisplayX(XYControlAssign xyp, float v) {
		invalidate(xyp.l, xyp.t, xyp.r, xyp.b);
		xyp.x = v;
		xyp.nx = xyp.xp.normalize(v);
		updateXYPosRect(xyp);
		invalidate(xyp.l, xyp.t, xyp.r, xyp.b);
	}

	public void updateDisplayY(XYControlAssign xyp, float v) {
		invalidate(xyp.l, xyp.t, xyp.r, xyp.b);
		xyp.y = v;
		xyp.ny = xyp.yp.normalize(v);
		updateXYPosRect(xyp);
		invalidate(xyp.l, xyp.t, xyp.r, xyp.b);
	}
	
}
