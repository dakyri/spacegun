package com.mayaswell.spacegun.fragment;

import com.mayaswell.fragment.SubPanelFragment;
import com.mayaswell.spacegun.SpaceGun;

import android.app.Activity;

public class SGSubPanelFragment extends SubPanelFragment {
	
	protected SpaceGun sg = null;

	@Override
	public void onAttach(Activity activity)
	{
		sg  = (SpaceGun) activity;
		super.onAttach(activity);
	}

}
