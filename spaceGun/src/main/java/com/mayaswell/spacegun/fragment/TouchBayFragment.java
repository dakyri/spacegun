package com.mayaswell.spacegun.fragment;

import com.mayaswell.audio.Controllable.XYControlAssign;
import com.mayaswell.spacegun.R;
import com.mayaswell.spacegun.widget.TouchPad;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TouchBayFragment extends SGSubPanelFragment {

	public TouchPad touchPad = null;
	
	public TouchBayFragment() {
		setGreedy();
		setPreferMaxArea();
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.d("touch frag", "creating view");
		View v = inflater.inflate(R.layout.touch_bay_layout, container, true);
		
		touchPad = (TouchPad) v.findViewById(R.id.touchPad);

		return v;
	}

	public void checkRedraw() {
		if (touchPad != null) {
			touchPad.invalidate();
		}
	}

	public void updateDisplayX(XYControlAssign xyp, float v) {
		if (touchPad != null) {
			touchPad.updateDisplayX(xyp, v);
		}
	}

	public void updateDisplayY(XYControlAssign xyp, float v) {
		if (touchPad != null) {
			touchPad.updateDisplayY(xyp, v);
		}
	}


}
