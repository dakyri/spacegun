package com.mayaswell.spacegun.fragment;

import com.mayaswell.spacegun.R;
import com.mayaswell.widget.InfinIntControl;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.text.Html;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;

public class SampleEditorFragment extends PadSampleEditorFragment {

	public SampleEditorFragment() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getPadActivity().getMenuInflater();
	    inflater.inflate(R.menu.sample_fragment_context_menu, menu);
		MenuItem miSampleTimeView = menu.findItem(R.id.sg_menu_show_sample_time_beats);
		if (miSampleTimeView != null) {
			miSampleTimeView.setVisible(true);
			miSampleTimeView.setChecked(getPadActivity().getShowSampleTimeBeats());
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item)
	{

		switch (item.getItemId()) {
		case R.id.sg_menu_show_sample_time_beats: {
			getPadActivity().setShowSampleTimeBeats(!getPadActivity().getShowSampleTimeBeats());
			return true;
		}
		case R.id.sg_menu_sample_help: {
			Builder d = new AlertDialog.Builder(getActivity());
			d.setTitle(getResources().getString(R.string.help_title_sample_edit));
			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_sample_edit)));
			d.setPositiveButton(getPadActivity().aboutButtonText(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
				}
			});
			d.show(); 
			return true;
		}
		}

		return false;
	}
}
