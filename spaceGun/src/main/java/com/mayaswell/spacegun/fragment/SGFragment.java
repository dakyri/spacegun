package com.mayaswell.spacegun.fragment;

import android.app.Activity;

import com.mayaswell.fragment.MWFragment;
import com.mayaswell.spacegun.SpaceGun;

public class SGFragment extends MWFragment {
	
	protected SpaceGun sg = null;

	@Override
	public void onAttach(Activity activity)
	{
		sg  = (SpaceGun) activity;
		super.onAttach(activity);
	}

}
