package com.mayaswell.spacegun.fragment;

import java.util.ArrayList;

import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable.ControllerInfo;
import com.mayaswell.audio.Controllable.RXControlAssign;
import com.mayaswell.audio.Controllable.SXControlAssign;
import com.mayaswell.audio.Controllable.SensorInfo;
import com.mayaswell.audio.Controllable.XControlAssign;
import com.mayaswell.audio.Controllable.XYControlAssign;
import com.mayaswell.spacegun.Patch;
import com.mayaswell.spacegun.R;
import com.mayaswell.spacegun.widget.OSCControl;
import com.mayaswell.widget.IPPortView;
import com.mayaswell.widget.MappedXMenuControl;
import com.mayaswell.widget.PadXYMenuControl;
import com.mayaswell.widget.TabWedgeIt;
import com.mayaswell.widget.MappedXMenuControl.MappedXMenuControlListener;
import com.mayaswell.widget.PadXYMenuControl.PadXYMenuControlListener;
import com.mayaswell.widget.TabWedgeIt.TabWedgeItListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.ViewAnimator;

public class SensorAssignFragment extends SGSubPanelFragment {
	private ArrayList<PadXYMenuControl> padXYControl = null;
	private ArrayList<MappedXMenuControl> senseXControl = null;
	private ArrayList<MappedXMenuControl> ribbonXControl = null;
	private ArrayList<OSCControl> oscParamControl = null;
	
	protected RelativeLayout touchEditorBody = null;
	protected RelativeLayout sensorEditorBody = null;
	protected RelativeLayout ribbonEditorBody = null;
	protected RelativeLayout oscEditorBody = null;

	protected TabWedgeIt controllerEditorTabs = null;
	protected ViewAnimator controllerEditorGroup = null;
	
	protected IPPortView oscRxPortEdit = null;
	
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		padXYControl = new ArrayList<PadXYMenuControl>();
		senseXControl = new ArrayList<MappedXMenuControl>();
		ribbonXControl = new ArrayList<MappedXMenuControl>();
		oscParamControl = new ArrayList<OSCControl>();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.controller_config_layout, container, true);

		touchEditorBody = (RelativeLayout) v.findViewById(R.id.touchEditorBody);
		sensorEditorBody = (RelativeLayout) v.findViewById(R.id.sensorEditorBody);
		ribbonEditorBody = (RelativeLayout) v.findViewById(R.id.ribbonEditorBody);
		oscEditorBody = (RelativeLayout) v.findViewById(R.id.oscEditorBody);
		oscRxPortEdit  = (IPPortView) v.findViewById(R.id.oscRxPortEdit);
		oscRxPortEdit.setListener(new IPPortView.Listener() {
			
			@Override
			public void portChanged(String host, String port, boolean enable) {
				sg.oscModule.setInputPort(port);
				sg.oscModule.setInputEnabled(enable);
			}

			@Override
			public void enableChanged(boolean b) {
				sg.oscModule.setInputEnabled(b);
			}


		});
		
		padXYControl.add((PadXYMenuControl) v.findViewById(R.id.padXYControl1));
		padXYControl.add((PadXYMenuControl) v.findViewById(R.id.padXYControl2));
		padXYControl.add((PadXYMenuControl) v.findViewById(R.id.padXYControl3));
		
		
		controllerEditorTabs = (TabWedgeIt) v.findViewById(R.id.controllerEditorGroupTabs);
		controllerEditorGroup  = (ViewAnimator) v.findViewById(R.id.controllerEditorGroup);
		controllerEditorTabs.setTabSelectTarget(controllerEditorGroup);
		
		controllerEditorTabs.add("touch", 0, touchEditorBody);
		controllerEditorTabs.add("ribbon", 1, ribbonEditorBody);
		controllerEditorTabs.add("sensor", 2, sensorEditorBody);
		controllerEditorTabs.add("OSC", 3, oscEditorBody);
		controllerEditorTabs.setListener(new TabWedgeItListener() {

			@Override
			public void onTabReselected(int ind, Object d) {
				if (ind == 3) {
					oscRxPortEdit.setCurrentValue(sg.oscModule.getHost(), Integer.toString(sg.oscModule.getPort()));
				}
			}

			@Override
			public void onTabSelected(int ind, Object d) {
				if (ind == 3) {
					oscRxPortEdit.setCurrentValue(sg.oscModule.getHost(), Integer.toString(sg.oscModule.getPort()));
				}
			}

			@Override
			public void onTabUnselected(int ind, Object d) {
			}
			
		});
				
		int pi=0;
		for (PadXYMenuControl p: padXYControl) {
			setPadXYListener(p, pi);
			pi++;
		}		
		
		registerForContextMenu(v);
		
		return v;
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		setToPatch(sg.getCurrentPatch());
//		layoutEditorControls();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = sg.getMenuInflater();
	    inflater.inflate(R.menu.sensor_fragment_context_menu, menu);
		MenuItem miCtrlClr = menu.findItem(R.id.sg_menu_clear_unused_sensor_ctrl);
		MenuItem miCtrlTouch = menu.findItem(R.id.sg_menu_add_touch_ctrl);
		MenuItem miCtrlSensor = menu.findItem(R.id.sg_menu_add_sensor_ctrl);
		MenuItem miCtrlRibbon = menu.findItem(R.id.sg_menu_add_ribbon_ctrl);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
		case R.id.sg_menu_add_touch_ctrl: {
			addPadXYControl(padXYControl.size());
			break;
		}
		case R.id.sg_menu_add_sensor_ctrl: {
			addSensorControl(senseXControl.size());
			break;
		}
		case R.id.sg_menu_add_ribbon_ctrl: {
			addRibbonControl(ribbonXControl.size());
			break;
		}
		case R.id.sg_menu_clear_unused_sensor_ctrl: {
			clearUnusedControllers();
			break;
		}
		case R.id.sg_menu_sensor_help: {
			Builder d = new AlertDialog.Builder(getActivity());
			if (isShowingTouchEditor()) {
				d.setTitle(getResources().getString(R.string.help_title_ctrl_edit));
				d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_ctrl_edit)));
			} else if (isShowingSensorEditor()) {
				d.setTitle(getResources().getString(R.string.help_title_sensor_edit));
				d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_sensor_edit)));
			} else if (isShowingRibbonEditor()) {
				d.setTitle(getResources().getString(R.string.help_title_ribbon_edit));
				d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_ribbon_edit)));
			} else if (isShowingOSCEditor()) {
				d.setTitle(getResources().getString(R.string.help_title_osc_edit));
				d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_osc_edit)));
			} else {
				d.setTitle(getResources().getString(R.string.help_title_general));
				d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_general)));
			}
			d.setPositiveButton(sg.aboutButtonText(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
				}
			});
			d.show(); 
			break;
		}
		}

		return false;
	}
	
	private void delSensorXControl(MappedXMenuControl p)
	{
		int pxi = senseXControl.indexOf(p);
		senseXControl.remove(pxi);
		sensorEditorBody.removeView(p);
		if (pxi < senseXControl.size()) {
			p = senseXControl.get(pxi);
			RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			if (pxi <= 0) {
				relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			} else {
				relativeParams.addRule(RelativeLayout.BELOW, senseXControl.get(pxi-1).getId());
			}
			p.setLayoutParams(relativeParams);
		} else {
			// we just deleted the last element, so no stress
		}
	}
	
	// TODO atm the padxy and ribbon editor don't inherit from the same base class as the bus fx and mod controls ... older ... should revise
	@Override
	public void layoutEditorControls()
	{
		
	}

	
	private void delPadXYControl(PadXYMenuControl p)
	{
		int pxi = padXYControl.indexOf(p);
		padXYControl.remove(pxi);
		touchEditorBody.removeView(p);
		if (pxi < padXYControl.size()) {
			p = padXYControl.get(pxi);
			RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			if (pxi <= 0) {
				relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			} else {
				relativeParams.addRule(RelativeLayout.BELOW, padXYControl.get(pxi-1).getId());
			}
			p.setLayoutParams(relativeParams);
		} else {
			if (senseXControl.size() > 0) {
				MappedXMenuControl s = senseXControl.get(0);
				RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.WRAP_CONTENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
				relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			}
		}
	}

	private void addPadXYControl(int pxi)
	{
		int mid = R.id.padXYControl1;
		PadXYMenuControl pxy0 = null;
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		if (padXYControl.size() <= 0) {
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		} else {
			pxy0 = padXYControl.get(padXYControl.size()-1);
			mid = pxy0.getId()+1;
			relativeParams.addRule(RelativeLayout.BELOW, pxy0.getId());
		}
		PadXYMenuControl pxy = new PadXYMenuControl(getActivity());
		pxy.setGravity(Gravity.CENTER_VERTICAL);
		pxy.setBackgroundResource(R.drawable.label_text_bg);
		int pxpd = getResources().getDimensionPixelSize(R.dimen.subElementPadding);
		pxy.setPadding(pxpd, pxpd, pxpd, pxpd);
		pxy.setName("Touch "+Integer.toString(pxi+1));
		pxy.setId(mid);
		setPadXYListener(pxy, pxi);
		
		padXYControl.add(pxy);
		touchEditorBody.addView(pxy, relativeParams);
		
	}

	private void setPadXYListener(PadXYMenuControl p, final int xyci)
	{
		p.setValueListener(new PadXYMenuControlListener() {
			@Override
			public void onValueChanged(PadXYMenuControl mapXYAssgnCtrl, Controllable cp, Control xp, Control yp)
			{
				Patch p = sg.getCurrentPatch();
				if (p != null) {
//					Log.d("pad assign", "mapping to controls");
					XYControlAssign newXYAssign = null;
					for (int i=0; i < p.touchAssign.size(); i++) {
						XYControlAssign xyAssgn = p.touchAssign.get(i); 
						if (i != xyci) {
							if (xyAssgn != null && xyAssgn.equals(cp, xp, yp)) {
								mapXYAssgnCtrl.clearAssignment();
//								sg.ribbonController.invalidate(); // XXX not sure ... should this not be invalidating touch?
								sg.invalidateTouchPad(xyAssgn);
								return;
							}
						} else {
							if (xyAssgn.equals(cp, xp, yp)) {
								return;
							}
							newXYAssign = xyAssgn;
						}
					}
					if (newXYAssign == null) {
						newXYAssign = new XYControlAssign(cp, xp, yp);
					} else {
						newXYAssign.setTo(cp, xp, yp);
					}
					mapXYAssgnCtrl.setControlAssign(newXYAssign);
				
//					Log.d("pad assign", String.format("%s %s", newXYAssign.controllable.toString(), newXYAssign.xp.abbrevName()));
					if (xyci >= p.touchAssign.size()) {
						for (int i=p.touchAssign.size(); i<=xyci; i++) {
							p.touchAssign.add(i, null);
						}
					}
					p.touchAssign.set(xyci, newXYAssign);
					
					newXYAssign.refreshNormalizedValue();
					sg.refreshTouchPad();
					
					checkAddEmptyPadAssign();
//					Log.d("pad assign", "all done");
				}
			}

		});
	}

	private void setTouchPadEditor(Patch p)
	{
		int pxi = 0;
		for (XYControlAssign xsp: p.touchAssign) {
			if (pxi >= padXYControl.size()) {
//				Log.d("adding px", String.format("got %d", pxi));
				addPadXYControl(pxi);
			}
			if (xsp != null) {
				padXYControl.get(pxi).setXYControlAssign(xsp);
			}
			pxi++;
		}
		for (int i=pxi; i<padXYControl.size(); i++) {
			padXYControl.get(i).clearAssignment();
		}
	}
	
	private void setSensorEditor(Patch p)
	{
		int pxi = 0;
		for (SXControlAssign xsp: p.sensorAssign) {
			if (pxi >= senseXControl.size()) {
				MappedXMenuControl snsx = addMappedXControl("SnsC", pxi, sg.sensorAdapter, sg.getControllableAdapter(), senseXControl, sensorEditorBody);
				setSenseXListener(snsx, pxi);
			}
			if (xsp != null) {
				MappedXMenuControl sex = senseXControl.get(pxi);
				sex.setRangeXControlAssign(xsp);
				sex.setController(xsp.sensor);
			}
			pxi++;
		}
		for (int i=pxi; i<senseXControl.size(); i++) {
			senseXControl.get(i).clearAssignment();
		}
	}

	private void setRibbonEditor(Patch p)
	{
		int pxi = 0;
		for (RXControlAssign xsp: p.ribbonAssign) {
			if (pxi >= ribbonXControl.size()) {
				MappedXMenuControl snsx = addMappedXControl("RbnC", pxi, sg.ribbonAdapter, sg.getControllableAdapter(), ribbonXControl, ribbonEditorBody);
				int px = getResources().getDimensionPixelSize(R.dimen.rxControllerSelectWidth);
				if (px > 0) {
					snsx.setControllerSelectWidth(px);
				}
				setRibbonXListener(snsx, pxi);
			}
			if (xsp != null) {
				ribbonXControl.get(pxi).setRangeXControlAssign(xsp);
			}
			pxi++;
		}
		for (int i=pxi; i<ribbonXControl.size(); i++) {
			ribbonXControl.get(i).clearAssignment();
		}
	}

	private void setOSCEditor(Patch p) {
		// TODO Auto-generated method stub
		
	}

	public void clearUnusedControllers()
	{
		Patch patch = sg.getCurrentPatch();
		for (int i=padXYControl.size()-1; i>= 0; i--) {
			PadXYMenuControl p = padXYControl.get(i);
			if (!p.inUse()) {
				delPadXYControl(p);
			}
		}
		
		for (int i=senseXControl.size()-1; i>= 0; i--) {
			MappedXMenuControl p = senseXControl.get(i);
			if (!p.inUse()) {
				delSensorXControl(p);
			}
		}
		
		for (int i=ribbonXControl.size()-1; i>= 0; i--) {
			MappedXMenuControl p = ribbonXControl.get(i);
			if (!p.inUse()) {
				delRibbonXControl(p);
			}
		}
	}
	
	private void setSenseXListener(MappedXMenuControl p, final int pi)
	{
		p.setValueListener(new MappedXMenuControlListener() {
			@Override
			public void onValueChanged(
					MappedXMenuControl mapXAssgnCtrl, ControllerInfo controller, Controllable cp, Control xp,
					float mapMin, float mapCenter, float mapMax, 
					float sMin, float sCenter, float sMax) {
				SensorInfo sensor = null;
				try {
					sensor = (SensorInfo) controller;
				} catch (Exception e) {
					
				}
				Patch p = sg.getCurrentPatch();
				if (p != null && sensor != null) {
					SXControlAssign newXAssign = null;
					for (int i=0; i < p.sensorAssign.size(); i++) {
						SXControlAssign xAssgn = p.sensorAssign.get(i); 
						if (i != pi) {
							if (xAssgn != null && xAssgn.equals(cp, xp)) {
								mapXAssgnCtrl.clearAssignment();
								sg.checkStartStopSensors();
								return;
							}
						} else {
							if (xAssgn.equals(cp, xp)) {
								return;
							}
							newXAssign = xAssgn;
						}
					}
					if (newXAssign == null) {
						newXAssign = new SXControlAssign(sensor, cp, xp);
					}
					newXAssign.setTo(sensor, cp, xp);
					newXAssign.setMapParams(mapMin, mapCenter, mapMax);
					newXAssign.setRangeParams(sMin, sCenter, sMax);
					mapXAssgnCtrl.setControlAssign(newXAssign);
				
					Log.d("sensor x", String.format("%s %s", newXAssign.controllable.toString(), newXAssign.xp.abbrevName()));
					if (pi >= p.sensorAssign.size()) {
						for (int i=p.sensorAssign.size(); i<=pi; i++) {
							p.sensorAssign.add(i, null);
						}
					}
					p.sensorAssign.set(pi, newXAssign);
					
					sg.checkStartStopSensors();
					newXAssign.refreshNormalizedValue();
					
					checkAddEmptySensorAssign();
				}
			}

			@Override
			public void onMapParamsChanged(MappedXMenuControl sx, float mapMin, float mapCenter, float mapMax)
			{
				XControlAssign xyc = sx.getControlAssign();
				if (xyc != null) {
					xyc.setMapParams(mapMin, mapCenter, mapMax);
				}
			}

			@Override
			public void onRangeParamsChanged(MappedXMenuControl sx,	float mapMin, float mapCenter, float mapMax)
			{
//				Log.d("sensor", String.format("%g %g %g", mapMin, mapCenter, mapMax));
				XControlAssign xyc = sx.getControlAssign();
				if (xyc != null) {
					xyc.setRangeParams(mapMin, mapCenter, mapMax);
				} else {
//					Log.d("sensor", "no controls assign");
				}
			}

		});
	}

	private void setRibbonXListener(MappedXMenuControl p, final int pi)
	{
		p.setValueListener(new MappedXMenuControlListener() {
			@Override
			public void onValueChanged(
					MappedXMenuControl mapXAssgnCtrl, ControllerInfo ribbon, Controllable cp, Control xp,
					float mapMin, float mapCenter, float mapMax, 
					float sMin, float sCenter, float sMax) {
				Patch p = sg.getCurrentPatch();
				if (p != null && ribbon != null) {
					RXControlAssign newXAssign = null;
					for (int i=0; i < p.ribbonAssign.size(); i++) {
						RXControlAssign xAssgn = p.ribbonAssign.get(i); 
						if (i != pi) {
							if (xAssgn != null && xAssgn.equals(cp, xp)) {
								mapXAssgnCtrl.clearAssignment();
								sg.ribbonController.invalidate();
								return;
							}
						} else {
							if (xAssgn.equals(cp, xp)) {
								return;
							}
							newXAssign = xAssgn;
						}
					}
					if (newXAssign == null) {
						newXAssign = new RXControlAssign(cp, xp);
					}
					newXAssign.setTo(cp, xp);
					newXAssign.setMapParams(mapMin, mapCenter, mapMax);
					newXAssign.setRangeParams(sMin, sCenter, sMax);
					mapXAssgnCtrl.setControlAssign(newXAssign);
				
					Log.d("ribbon x", String.format("%s %s", newXAssign.controllable.toString(), newXAssign.xp.abbrevName()));
					if (pi >= p.ribbonAssign.size()) {
						for (int i=p.ribbonAssign.size(); i<=pi; i++) {
							p.ribbonAssign.add(i, null);
						}
					}
					p.ribbonAssign.set(pi, newXAssign);
					
					newXAssign.refreshNormalizedValue();
					sg.ribbonController.invalidate();
					checkAddEmptyRibbonAssign();
				}
			}

			@Override
			public void onMapParamsChanged(MappedXMenuControl sx, float mapMin, float mapCenter, float mapMax)
			{
				XControlAssign xyc = sx.getControlAssign();
				if (xyc != null) {
//					Log.d("ribbon assign", String.format("map change %g %g %g", mapMin, mapCenter, mapMax));
					xyc.setMapParams(mapMin, mapCenter, mapMax);
					sg.ribbonController.invalidate();
				}
			}

			@Override
			public void onRangeParamsChanged(MappedXMenuControl sx,	float mapMin, float mapCenter, float mapMax)
			{
				XControlAssign xyc = sx.getControlAssign();
				if (xyc != null) {
//					Log.d("ribbon assign", String.format("range change %g %g %g", mapMin, mapCenter, mapMax));
					xyc.setRangeParams(mapMin, mapCenter, mapMax);
					sg.ribbonController.invalidate();
				}
			}

		});
	}

	private void checkAddEmptyPadAssign() {
		boolean hasUnassigned = false;
		for (PadXYMenuControl p: padXYControl) {
			if (!p.inUse()) {
				hasUnassigned = true;
				break;
			}
		}
		if (!hasUnassigned) {
			addPadXYControl(padXYControl.size());
		}
	}
	
	private void checkAddEmptySensorAssign() {
		boolean hasUnassigned = false;
		for (MappedXMenuControl p: senseXControl) {
			if (!p.inUse()) {
				hasUnassigned = true;
				break;
			}
		}
		if (!hasUnassigned) {
			addSensorControl(senseXControl.size());
		}
	}

	private void checkAddEmptyRibbonAssign() {
		boolean hasUnassigned = false;
		for (MappedXMenuControl p: ribbonXControl) {
			if (!p.inUse()) {
				hasUnassigned = true;
				break;
			}
		}
		if (!hasUnassigned) {
			addRibbonControl(ribbonXControl.size());
		}
	}

	private void checkAddEmptyOSCAssign() {
		// TODO Auto-generated method stub
		
	}

	public void addSensorControl(int snsxi) {
		MappedXMenuControl snsx = addMappedXControl("SnsC", snsxi, sg.sensorAdapter, sg.getControllableAdapter(), senseXControl, sensorEditorBody);
		setSenseXListener(snsx, snsxi);
	}

	public void addRibbonControl(int snsxi) {
		MappedXMenuControl snsx = addMappedXControl("RbnC", snsxi, sg.ribbonAdapter, sg.getControllableAdapter(), ribbonXControl, ribbonEditorBody);
		int px = getResources().getDimensionPixelSize(R.dimen.rxControllerSelectWidth);
		if (px > 0) {
			snsx.setControllerSelectWidth(px);
		}
		setRibbonXListener(snsx, snsxi);
	}

	public boolean isShowingTouchEditor() {
		return controllerEditorGroup.getCurrentView() == touchEditorBody;
	}
	public boolean isShowingRibbonEditor() {
		return controllerEditorGroup.getCurrentView() == ribbonEditorBody;
	}
	public boolean isShowingSensorEditor() {
		return controllerEditorGroup.getCurrentView() == sensorEditorBody;
	}

	public boolean isShowingOSCEditor() {
		return controllerEditorGroup.getCurrentView() == oscEditorBody;
	}

	public void setToPatch(Patch p) {
		if (p == null || getView() == null) return;
		setTouchPadEditor(p);
		setSensorEditor(p);
		setRibbonEditor(p);
		setOSCEditor(p);
		
		checkAddEmptyPadAssign();
		checkAddEmptyRibbonAssign();
		checkAddEmptySensorAssign();
		checkAddEmptyOSCAssign();
	}

	private MappedXMenuControl addMappedXControl(String baseName, int snsxi, SpinnerAdapter ctrllerAdapter, SpinnerAdapter ctrlableAdapter,
			ArrayList<MappedXMenuControl> list, ViewGroup body)
	{
		int mid = 1;
		MappedXMenuControl sxy0 = null;
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		if (list.size() <= 0) {
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		} else {
			sxy0 = list.get(list.size()-1);
			mid = sxy0.getId()+1;
			relativeParams.addRule(RelativeLayout.BELOW, sxy0.getId());
		}
		MappedXMenuControl snsx = new MappedXMenuControl(getActivity());
		snsx.setMainAdapters(ctrllerAdapter, ctrlableAdapter);
		snsx.setGravity(Gravity.CENTER_VERTICAL);
		snsx.setBackgroundResource(R.drawable.label_text_bg);
		int pxpd = getResources().getDimensionPixelSize(R.dimen.subElementPadding);
		snsx.setPadding(pxpd, pxpd, pxpd, pxpd);
		snsx.setName(baseName+Integer.toString(snsxi+1));
		//noinspection ResourceType
		snsx.setId(mid);
		
		list.add(snsx);
		body.addView(snsx, relativeParams);
		
		return snsx;
	}
	
	private void delRibbonXControl(MappedXMenuControl p) {
		// TODO Auto-generated method stub
		
	}

}
