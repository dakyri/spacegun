package com.mayaswell.spacegun;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import android.util.Log;

import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.mayaswell.spacegun.CControl;
import com.mayaswell.spacegun.widget.PadButton;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.OSCModule;

public class SGOSCModule extends OSCModule {
	protected SpaceGun spaceGun = null;
	protected SGGlobalListener globalListener = null;
	
	public class SGGlobalListener implements OSCListener
	{
		public SGGlobalListener() {
		}

		@Override
		public void acceptMessage(Date date, OSCMessage msg) {
//			Log.d("OSC", "SGGlobalListener "+msg.getAddress());
			String path=msg.getAddress();
			String leaves[] = path.split("/");
			Object [] args = msg.getArguments();
			if (!leaves[1].equals("sg")) {
				return;
			}
			if (leaves.length <= 3) { /** "/sg/*" */
				if (args.length < 1) {
					return;
				}
				float p;
				if (args.length > 0 && args[0] instanceof Float) {
					p = ((Float)args[0]).floatValue();
				} else {
					return;
				}
				if (leaves[2].equals("tempo")) {
					spaceGun.setTempo(p); // TODO doesn't update interface
				}
			} else {
				if (leaves[2].equals("pad")) { /** "/sg/pad/[0-9]/*" */
					int pid = 0;
					try {
						pid = Integer.parseInt(leaves[3]);
					} catch (NumberFormatException e) {
						Log.d("OSC", "pad "+leaves[3]+" not found");
						return;
					}
					final PadSample ps = spaceGun.padForId(pid);
					if (ps == null) {
						Log.d("OSC", "pad "+pid+" not found");
						return;
					}
					if (leaves[4].equals("start")) {
						spaceGun.runOnUiThread(new Runnable() {
							@Override public void run() {
								PadButton pb = null;
								try {
									pb = (PadButton) ps.padButton;
								} catch (ClassCastException e) {
									
								}
								spaceGun.startPad(pb);
							}							
						});		
						return;
					} else if (leaves[4].equals("stop")) {
						spaceGun.runOnUiThread(new Runnable() {
							@Override public void run() {
								PadButton pb = null;
								try {
									pb = (PadButton) ps.padButton;
								} catch (ClassCastException e) {
									
								}
								spaceGun.stopPad(pb);
							}							
						});
						return;
					} else if (leaves[4].equals("record")) {
						spaceGun.runOnUiThread(new Runnable() {
							@Override public void run() {
								PadButton pb = null;
								try {
									pb = (PadButton) ps.padButton;
								} catch (ClassCastException e) {
									
								}
								spaceGun.startRecording(pb);
							}							
						});	
						return;
					}
					
					final int cid = CControl.string2code(leaves[4]);
					if (cid == CControl.NOTHING) {
						Log.d("OSC", "pad "+pid+" control "+leaves[4]+" not found");
						return;
					}
					CControl cc = new CControl(cid);
					if (args.length < 1) {
						return;
					}
					if (args[0] instanceof Float) {
						final float v = ((Float)args[0]).floatValue();
						spaceGun.runOnUiThread(new Runnable() {
							@Override public void run() { spaceGun.ccontrolUpdate(ps, cid, v, Controllable.Controller.OSC); }
						});
						ps.setValue(cc, v);
					}
				} else if (leaves[2].equals("bus")) {
					int bid = 0;
					try {
						bid = Integer.parseInt(leaves[3]);
					} catch (NumberFormatException e) {
						Log.d("OSC", "bus "+leaves[3]+" not found");
						return;
					}
					final Bus bs = BusMixer.getBus(bid);
					if (bs == null) {
						Log.d("OSC", "bus"+bid+" not found");
						return;
					}
					if (leaves.length == 5) { /** "/sg/bus/[0-9]/*" */
						final int cid = CControl.string2code(leaves[4]);
						if (cid == CControl.NOTHING) {
							Log.d("OSC", "pad "+bid+" control "+leaves[4]+" not found");
							return;
						}
						CControl cc = new CControl(cid);
						if (args.length < 1) {
							return;
						}
						final float v;
						if (args[0] instanceof Float) {
							v = ((Float)args[0]).floatValue();
							spaceGun.runOnUiThread(new Runnable() {
								@Override public void run() { spaceGun.ccontrolUpdate(bs, cid, v, Controllable.Controller.OSC); }
							});
							bs.setValue(cc, v);
						}
					} else if (leaves.length == 7) { /** "/sg/bus/[0-9]/fx/[0-9]/[0-9]" */
						if (leaves[4].equals("fx")) {
							int fid = 0;
							int pid = 0;
							try {
								fid = Integer.parseInt(leaves[5]);
							} catch (NumberFormatException e) {
								Log.d("OSC", "fx "+fid+" control "+leaves[5]+" not found");
								return;
							}
							try {
								pid = Integer.parseInt(leaves[6]);
							} catch (NumberFormatException e) {
								Log.d("OSC", "fx "+pid+" control "+leaves[6]+" not found");
								return;
							}
							final int cid = CControl.fxParamId(fid, pid);
							CControl cc = new CControl(cid);
							if (args.length < 1) {
								return;
							}
							final float v;
							if (args[0] instanceof Float) {
								v = ((Float)args[0]).floatValue();
								spaceGun.runOnUiThread(new Runnable() {
									@Override public void run() { spaceGun.ccontrolUpdate(bs, cid, v, Controllable.Controller.OSC); }
								});
								bs.setValue(cc, v);
							}
							
						}
					}
				}
			} 
		}
	}
	
	public SGOSCModule(SpaceGun sg)
	{
		super();
		spaceGun = sg;
		globalListener = new SGGlobalListener();
		listeners.put("/sg/.*", globalListener);

		Log.d("OSC", "ip = "+getIPAddress(true));
	}
}
