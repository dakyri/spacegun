package com.mayaswell.spacegun;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import com.mayaswell.audio.AudioMixer;

public class SGAudioMixer extends AudioMixer
{	
	
	private PadSample activePad[]=null;
	private int nActivePad = 0;
		
	
	public SGAudioMixer(int n)
	{
		super();
		activePad = new PadSample[n];
		nActivePad = 0;
	}
	
	public boolean play()
	{
		Thread t; 
		
		if (isPlaying) {
			return false;
		}
		
		t = new Thread() {
			public void run() {
				isPlaying = true;
				
				setPriority(Thread.MAX_PRIORITY);

				AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, dfltSampleRate, 
							outChannelFormat, 
							AudioFormat.ENCODING_PCM_16BIT, 
						 	outBufSize, 
						 	AudioTrack.MODE_STREAM);
			
				int nFramePerBuf = outBufSize/nOutChannels;
//				float busBuffer[] = new float[outBufSize];
				short outBuffer[] = new short[outBufSize];

				audioTrack.play();
				
/*				File appDir = getStorageBaseDir();
				WavFile dumpFile = null;
				try {
					dumpFile = WavFile.newWavFile(new File(appDir, "dump.wav"), nOutChannels, Long.MAX_VALUE, 16, sampleRate);
				} catch (IOException e) {
//					dispatchMafError("Catastrophe! Some kind of IO exception, creating record file "+file.getPath());
				} catch (AudioFileException e) {
//					dispatchMafError("How Bizzare! An audio file exception, creating record file "+file.getPath());
				}*/
				int nActiveBus = 1;
				while(isPlaying){
					/*
					for(int i=0; i < outBufSize; i++) {
						busBuffer[i] = 0;
					}*/
					BusMixer.zeroBuffers(nFramePerBuf);
					int j = 0;
					int ac = 0;
//					nActiveBus = 1; // lets keep bus 1 on for now
					while (j<activePad.length && ac<nActivePad) {
						PadSample p = activePad[j];
						if (p != null) {
							p.playCumulative(BusMixer.getNativePointer()/*busBuffer*/, nFramePerBuf, (short) 2);
							int pab = p.nActiveBus();
							if (pab > nActiveBus) {
								nActiveBus = pab;
							}
							ac++;
						}
						j++;
					}
					/*
					for(int i=0; i < outBufSize; i++) {
						float b=busBuffer[i];
						outBuffer[i] = (short)(Short.MAX_VALUE*(b<-1.0f?-1.0f:(b>1.0f?1.0f:b)));
					}*/
					BusMixer.mix(nActiveBus, outBuffer, nFramePerBuf, (short)nOutChannels);
					audioTrack.write(outBuffer, 0, outBufSize);
					
					/*try {
						if (dumpFile != null) dumpFile.writeFrames(outBuffer, outBufSize/nOutChannels);
					} catch (IOException e) {
//						dispatchMafError("Catastrophe! Some kind of IO exception, writing record file "+file.getPath());
//						break;
					} catch (AudioFileException e) {
//						dispatchMafError("How Bizzare! An audio file exception, writing record file "+file.getPath());
//						break;
					}*/
				}
				
				/*if (dumpFile != null)
					try {
						dumpFile.close();
					} catch (IOException e) {
				}*/
				audioTrack.stop();
				audioTrack.release();
			}
		};
		
		t.start();
		return true;
	}
	
	public synchronized boolean startPad(PadSample p)
	{
		return startPad(p, false);
	}
	
	public synchronized boolean startPad(PadSample p, boolean fromSync)
	{
		if (p == null) {
			return false;
		}
		if (!p.isAssigned()) {
			return false;
		}
		int j = 0;
		int ac = 0;
		int ah = -1;
		while (j<activePad.length && ac < nActivePad) {
			if (activePad[j] == p) {
				p.fire();
				return true;
			}
			if (activePad[j] != null) {
				ac++;
			} else {
				if (ah < 0) ah = j;
			}
			j++;
		}
		if (nActivePad < activePad.length) {
			if (ah >= 0) {
				activePad[ah] = p;
			} else {
				activePad[nActivePad] = p;
			}
			nActivePad++;
			p.fire(fromSync); // NB this may fire off other pads now!!!! order of these statements is importan increment nActive first
		}
		return true;
	}
	
	public synchronized void stopPad(PadSample p)
	{
		int j = 0;
		int ac = 0;
		while (j<activePad.length && ac<nActivePad) {
			if (activePad[j] == p) {
				activePad[j] = null;
				nActivePad--;
				return;
			}
			if (activePad[j] != null) {
				ac++;
			}
			j++;
		}
	}
	
	public boolean isPlaying(PadSample p)
	{
		if (p == null) return false;
		if (!p.isPlaying()) return false;
		/*
		for (int j=0; j<nActivePad; j++) {
			if (activePad[j] == p) return true;
		}
		*/
		int j = 0;
		int ac = 0;
		while (j<activePad.length && ac<nActivePad) {
			if (activePad[j] == p) {
				return true;
			}
			if (activePad[j] != null) {
				ac++;
			}
			j++;
		}
		return false;
	}
	
	public void stop()
	{
		isPlaying = false;
	}

}
